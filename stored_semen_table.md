## stored_semen_table
```SQL
CREATE TABLE "stored_semen_table" 
	("id_storedsemenid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NUll UNIQUE
	, "id_animalid" INTEGER NOT NUll
	, "number_straws" INTEGER NOT NUll
	, "straws_per_insemination" TEXT NOT NUll 
	, "id_semenextenderid" INTEGER NOT NUll
	, "post_thaw_motility" REA NOT NUll
	, "post_thaw_progressive_motility" REAL NOT NUll
	, "motile_sperm_per_insemination" INTEGER NOT NUll
	, "goblet_identifier" TEXT NOT NUll
	, "date_collected" TEXT NOT NUll
	, "time_collected" TEXT NOT NUll
	, "owner_id_contactsid" INTEGER NOT NUll
	, "location_id_contactsid" INTEGER NOT NUll
	, "id_animalbreedid" INTEGER NOT NUll
	);
```