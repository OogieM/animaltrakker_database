## temp_registration_cross_ref_table
This is being used as part of the conversion of the ABWMSA registry database out of Grassroots and into AnimalTrakker and is a table in the LambTracker database structure. Will not be part of the normal AnimalTrakker database system. 
```SQL
CREATE TABLE "temp_registration_cross_ref_table" 
	("id_tempcrossrefid" INTEGER PRIMARY KEY  NOT NULL 
	, "id_animalid" INTEGER NOT NULL
	, "animal_reg_num" TEXT NOT NULL
	, "sire_reg_num" TEXT NOT NULL
	, "dam_reg_num" TEXT NOT NULL
	);
```