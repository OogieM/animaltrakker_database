## drug_table
```SQL
CREATE TABLE "drug_table" 
	("id_drugid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NUll UNIQUE
	, "id_drugtypeid" INTEGER NOT NUll
	, "official_drug_name" TEXT NOT NUll
	, "generic_drug_name" TEXT NOT NUll
	, "drug_lot" TEXT NOT NUll
	, "drug_expire_date" TEXT NOT NUll
	, "drug_purchase_date" TEXT NOT NUll
	, "drug_amount_purchased" TEXT NOT NUll
	, "drug_cost" REAL NOT NUll
	, "id_drug_cost_id_unitsid" INTEGER NOT NUll
	, "drug_dispose_date" TEXT NOT NUll
	, "drug_gone" INTEGER NOT NUll
	, "drug_removable" INTEGER NOT NUll
	, "drug_meat_withdrawal" INTEGER NOT NUll
	, "id_meat_withdrawal_id_unitsid" INTEGER NOT NUll
	, "user_meat_withdrawal" INTEGER NOT NUll
	, "drug_milk_withdrawal" INTEGER NOT NUll
	, "id_milk_withdrawal_id_unitsid" INTEGER NOT NUll
	, "user_milk_withdrawal" INTEGER NOT NUll
	, "official_drug_dosage" TEXT NOT NUll
	, "off_label" INTEGER NOT NUll
	, "off_label_vet_id_contactsid" INTEGER NOT NUll
	, "user_drug_dosage" TEXT NOT NUll
	, "user_task_name" TEXT NOT NUll
	);
```