## evaluation_trait_table
```SQL
CREATE TABLE "evaluation_trait_table" 
	("id_evaluationtraitid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "trait_name" TEXT NOT NULL
	, "id_evaluationtraittypeid" INTEGER NOT NULL
	, "evaluation_trait_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults 
```SQL
INSERT INTO evaluation_trait_table(id_evaluationtraitid,trait_name,id_evaluationtraittypeid,evaluation_trait_display_order) VALUES (1,'Scrotal Circumference in cm',2,1);
INSERT INTO evaluation_trait_table(id_evaluationtraitid,trait_name,id_evaluationtraittypeid,evaluation_trait_display_order) VALUES (2,'Sperm Motility % Motile',2,2);
INSERT INTO evaluation_trait_table(id_evaluationtraitid,trait_name,id_evaluationtraittypeid,evaluation_trait_display_order) VALUES (3,'Sperm Morphology % Normal',2,3);
INSERT INTO evaluation_trait_table(id_evaluationtraitid,trait_name,id_evaluationtraittypeid,evaluation_trait_display_order) VALUES (4,'Breeding Soundness Exam',3,4);
INSERT INTO evaluation_trait_table(id_evaluationtraitid,trait_name,id_evaluationtraittypeid,evaluation_trait_display_order) VALUES (5,'Weight',2,6);
INSERT INTO evaluation_trait_table(id_evaluationtraitid,trait_name,id_evaluationtraittypeid,evaluation_trait_display_order) VALUES (6,'Body Condition Score',2,5);

```