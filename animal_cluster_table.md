## animal_cluster_table
```SQL
--	Clusters are bloodlines and can be calculated or defined
CREATE TABLE "animal_cluster_table" (
	"id_animalclusterid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL
	, "cluster_date" TEXT NOT NULL
	, "id_clusternameid" INTEGER  NOT NULL
	)
```