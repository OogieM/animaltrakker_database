## custom_evaluation_traits_table
```SQL
CREATE TABLE "custom_evaluation_traits_table" 
	("id_custom_evaluationtraitsid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_traitid" INTEGER NOT NULL
	, "custom_evaluation_item" TEXT NOT NULL
	, "custom_evaluation_order" INTEGER NOT NULL
	);
```

```