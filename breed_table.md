## breed_table
```SQL
CREATE TABLE "breed_table" 
	("id_breedid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "breed_name" TEXT NOT NULL
	, "breed_abbrev" TEXT NOT NULL
	, "breed_display_order" INTEGER NOT NULL
	, "registry_id_contactsid" INTEGER NOT NULL
	, "id_speciesid" INTEGER NOT NULL
	)
```

Insert statements to pre-fill the table with defaults.
```SQL
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (1,'Black Welsh Mountain','BW',1,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (2,'Crossbred','X',5,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (3,'Shetland','SL',6,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (4,'Chocolate Welsh Mountain','CH',3,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (5,'White Welsh Mountain','WW',4,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (6,'Unknown','UN',7,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (7,'Black Welsh Mountain-UK','BW-UK',2,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (8,'Black Angus','AN',1,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (9,'Red Angus','AR',2,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (10,'Charolais','CH',3,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (11,'Gelbvieh','GV',4,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (12,'Hereford Polled','HP',5,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (13,'Holstein','HO',6,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (14,'Simmental','SM',7,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (15,'Limousin','LM',8,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (16,'Black Baldy','Bk Baldy',9,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (17,'Composite','Comp',10,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (18,'Balancer','Bal',11,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (19,'Unknown','UNK',12,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (20,'Pinzgauer','PZ',13,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (21,'Crossbred','XX',14,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (22,'Canadienne','Can',15,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (23,'Milking Devon','DE',16,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (24,'Randall Lineback','RL',17,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (25,'Texas Longhorn','TL',18,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (26,'Florida Cracker','FC',19,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (27,'Pineywoods','PN',20,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (28,'Dutch Belted','DL',21,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (29,'Shorthorn Beef','SS',22,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (30,'Red Poll','RP',23,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (31,'Guernsey','GU',24,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (32,'Dexter','DR',25,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (33,'Red Devon','RV',26,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (34,'Kerry','KR',27,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (35,'Lincoln Red','LR',28,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (36,'Ancient White Park','WP',29,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (37,'Belted Galloway','BG',30,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (38,'Ankole-Watusi','AW',31,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (39,'Florida Cracker','FC',59,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (40,'Gulf Coast','GC',8,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (41,'Hog Island','HG',9,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (42,'Navajo Churro','NV',10,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (43,'Santa Cruz','SZ',11,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (44,'Jacob','JA',12,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (45,'Karakul','KK',13,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (46,'Romedale/CVM','RD',14,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (47,'Barbados Blackbelly','LY',15,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (48,'St. Croix','SX',16,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (49,'Tunis','TU',17,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (50,'Cotswold','DW',18,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (51,'Dorset Horn','DH',19,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (52,'Lincoln','LI',20,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (53,'Oxford','OX',21,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (54,'Shropshire','SR',22,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (55,'Southdown','ST',23,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (56,'Teeswater','TW',24,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (57,'Soay','SY',25,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (58,'Clun Forest','CF',26,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (59,'Wiltshire Horn','WH',27,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (60,'Blue Faced Leister','BF',28,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (61,'Border Cheviot','BC',29,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (62,'Charollais','CO',30,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (63,'Columbia','CL',31,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (64,'Coopworth','CP',60,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (65,'Corriedale','CR',32,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (66,'Dorper','DO',33,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (67,'Drysdale','DY',34,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (68,'East Friesian','EF',35,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (69,'Finnish Landrace','FN',36,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (70,'Hampshire','HS',37,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (71,'Icelandic','IL',38,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (72,'Ile de France','IF',39,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (73,'Leicester Longwool','LL',40,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (74,'Katahdin','KA',41,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (75,'Kerry Hill','KH',42,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (76,'Lacaune Dairy Sheep','CU',43,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (77,'Border Leicester','BL',44,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (78,'Merino','MM',45,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (79,'Merino Polled','MP',46,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (80,'Montadale','MT',47,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (81,'North Country Cheviot','NC',48,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (82,'Perendale','PE',49,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (83,'Polypay','PP',50,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (84,'Rambouillet','RG',51,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (85,'Romanov','RV',52,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (86,'Romney','RY',53,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (87,'Ryeland','RL',54,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (88,'Scottish Blackface','SC',55,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (89,'Suffolk','SU',56,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (90,'Targhee','TA',57,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (91,'Texel','TX',58,1,1);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (92,'Ayrshire','AY',32,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (93,'Beefalo','BE',33,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (94,'Beefmaster','BM',34,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (95,'Belgian Blue','BB',35,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (96,'Brahman','BR',36,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (97,'Brangus','BN',37,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (98,'Brown Swiss','BS',38,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (99,'Chianina','CA',39,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (100,'Hereford Horned','HH',40,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (101,'Highland Scotch','SH',41,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (102,'Jersey','JE',42,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (103,'Mexican Corriente','MC',43,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (104,'Murray Grey','MG',44,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (105,'Normande','NM',45,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (106,'Norwegian Red','NR',46,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (107,'Piedmontese','PI',47,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (108,'Red Holstein','WW',48,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (109,'Salers','SA',49,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (110,'Santa Gertrudis','SG',50,1,3);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (111,'Alpine','AI',1,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (112,'Angora','AG',2,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (113,'Arapawa','AR',3,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (114,'Boer','BZ',4,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (115,'Cashmere','CS',5,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (116,'La Mancha','LN',6,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (117,'Myotonic','MY',7,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (118,'Nigerian Dwarf','ND',8,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (119,'Nubian','NU',9,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (120,'Oberhasli','OH',10,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (121,'Pygmy','PY',11,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (122,'Saanen','EN',12,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (123,'San Clemente Island','SC',13,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (124,'Spanish','SP',14,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) 
VALUES (125,'Toggenburg','TO',15,1,2);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (126,'Berkshire','BK',1,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (127,'Chester White','CW',2,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (128,'Choctaw','CH',3,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (129,'Duroc','DU',4,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (130,'Gloucestershire Old Spots','GS',5,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (131,'Guinea Hog','GH',6,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (132,'Hampshire','HA',7,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (133,'Hereford','HE',8,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (134,'Lacombe','LC',9,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (135,'Landrace','LA',10,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (136,'Large Black','LB',11,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (137,'Large White','LW',12,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (138,'Meishan','MS',13,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (139,'Mulefoot','MF',14,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (140,'Ossabaw Island','OI',15,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (141,'Pietrain','PE',16,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (142,'Poland China','PC',17,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (143,'Red Wattle','RW',18,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (144,'Saddleback','SB',19,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (145,'Spotted','SO',20,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (146,'Tamworth','TM',21,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (147,'Wessex Saddleback','WS',22,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (148,'Yorkshire','YO',23,1,6);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (149,'American Mammoth','AMD',1,1,5);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (150,'Poitou','POD',2,1,5);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (151,'Miniature','MND',3,1,5);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (152,'Standard','DOK',4,1,5);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (153,'Wild Burro','WDB',5,1,5);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (154,'Mule ','MUL',6,1,5);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (155,'Akhal-Teke','AKT',1,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (156,'American Bashkir Curley','ABC',2,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (157,'American Cream','CD',3,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (158,'American Miniature','AMH',4,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (159,'American Saddlebred','AS',5,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (160,'Andalusian','AA',6,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (161,'Appaloosa','AP',7,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (162,'Arabian','AD',8,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (163,'Banker','BAK',9,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (164,'Barb','BAR',10,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (165,'Bashkir Curly','BAC',11,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (166,'Bavarian Warmblood','BY',12,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (167,'Belgian','GI',13,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (168,'Brabant','BB',14,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (169,'Canadian','CI',15,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (170,'Caspian','CAS',16,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (171,'Chincoteague Pony','CHP',17,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (172,'Cleveland Bay','CV',18,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (173,'Clydesdale','CY',19,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (174,'Colonial Spanish','CS',20,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (175,'Colorado Ranger Horse','CRH',21,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (176,'Connemara Pony','CM',22,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (177,'Dales Pony','DAP',23,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (178,'Danish Warmblood','DW',24,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (179,'Dartmoor','DT',25,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (180,'Dutch Warmblood','DWB',26,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (181,'Exmoor','EX',27,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (182,'Fell Pony','FE',28,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (183,'Finnhhorse','FIN',29,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (184,'Fjord','FJ',30,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (185,'Florida Cracker','FLC',31,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (186,'Friesan','FR',32,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (187,'Galiceno','GAL',33,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (188,'German Warmblood','GER',34,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (189,'Gotland','GOT',35,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (190,'Hackney Horse','HN',36,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (191,'Hackney Pony','HK',37,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (192,'Haflinger','HF',38,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (193,'Hanoverian','HV',39,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (194,'Highland Pony','HG',40,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (195,'Icelandic','IC',41,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (196,'Irish Draught','IRD',42,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (197,'Jutland','JUT',43,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (198,'Lipizzan','LZ',44,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (199,'Lusitano','LUS',45,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (200,'Marsh Tacky','MT',46,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (201,'Miniature Horse','MNH',47,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (202,'Morab','MOB',48,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (203,'Morgan','MND',49,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (204,'National Show Horse','NAT',50,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (205,'New Forest Pony','NF',51,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (206,'Newfoundland Pony','NFP',52,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (207,'Oldenburg','OB',53,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (208,'Orlov Trotter','ORT',54,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (209,'Paint','PAI',55,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (210,'Palomino','PL',56,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (211,'Paso Fino','PF',57,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (212,'Percheron','PH',58,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (213,'Peruvian Paso','PV',59,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (214,'Pinto','PN',60,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (215,'Polish Warmblood','PW',61,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (216,'Pony','PO',62,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (217,'Pony of the Americas','PAI',63,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (218,'Quarter Horse','QH',64,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (219,'Quarter Pony','QHP',65,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (220,'Racking Horse','RAK',66,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (221,'Rocky Mountain','RM',67,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (222,'Shetland','SE',68,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (223,'Shire','SY',69,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (224,'Spanish Baca Chica','SBC',70,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (225,'Spanish Choctaw','SCH',71,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (226,'Spanish Santa Cruz','SSC',72,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (227,'Spanish Sulphur','SS',73,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (228,'Spanish Wilbur-Cruce','SWC',74,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (229,'Standardbred','SN',75,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (230,'Suffolk','SF',76,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (231,'Swedish Warmblood','IW',77,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (232,'Tennessee Walker','TW',78,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (233,'Thoroughbred','TH',79,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (234,'Trakehner','TR',80,1,4);
INSERT INTO breed_table(id_breedid,breed_name,breed_abbrev,breed_display_order,registry_id_contactsid,id_speciesid) VALUES (235,'Welsh Pony','WP',81,1,4);
INSERT INTO 
```