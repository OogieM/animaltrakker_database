## animal_external_file_table
```SQL
CREATE TABLE "animal_external_file_table" 
	( "id_animalexternalfileid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL
	, "animal_external_file_name" TEXT NOT NULL
	, "animal_external_file_path_from_root" TEXT NOT NULL
	, "animalexternal_file_absolute_path" TEXT NOT NULL
	, "animalexternal_file_hash" TEXT NOT NULL
	, "animalexternal_file_date" TEXT NOT NULL
	, "animalexternal_file_time" TEXT NOT NULL
	, "id_animalexternalfiletypeid" INTEGER NOT NULL
	)
```