# sheep_weaning_adjustment_table
Used in sheep to adjust the weaning weight to account of age of dam, sex of lamb and birth and rear types.
```SQL
CREATE TABLE "sheep_weaning_adjustment_table" 
	("id_sheepweaningadjustmentid" INTEGER PRIMARY KEY
	, "adjustment_name" TEXT NOT NULL
	, "ewe_age_1yr" REAL NOT NULL
	, "ewe_age_2yr" REAL NOT NULL
	, "ewe_age_3_6yr" REAL NOT NULL
	, "ewe_age_over_6yr" REAL NOT NULL
	, "lamb_sex_ram" REAL NOT NULL
	, "lamb_sex_ewe" REAL NOT NULL
	, "lamb_sex_wether" REAL NOT NULL
	, "birth_and_rear_11" REAL NOT NULL
	, "birth_and_rear_12" REAL NOT NULL
	, "birth_and_rear_21" REAL NOT NULL
	, "birth_and_rear_22" REAL NOT NULL
	, "birth_and_rear_31" REAL NOT NULL
	, "birth_and_rear_32" REAL NOT NULL
	, "birth_and_rear_33" REAL NOT NULL
	, "source_reference" TEXT NOT NULL
	);
```

Insert statements to pre-fill the table with defaults 
```SQL
INSERT INTO sheep_weaning_adjustment_table(id_sheepweaningadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3_6yr, ewe_age_over_6yr, lamb_sex_ram, lamb_sex_ewe, lamb_sex_wether, birth_and_rear_11, birth_and_rear_12, birth_and_rear_21, birth_and_rear_22, birth_and_rear_31, birth_and_rear_32, birth_and_rear_33, source_reference ) 
VALUES(1,'Generic',1.14,1.08,1,1.05,0.91,1,0.97,1,1.17,1.11,1.21,1.19,1.29,1.36,'2015 Vol. 8 edition of the Sheep Production Manual page 52');
INSERT INTO sheep_weaning_adjustment_table(id_sheepweaningadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3_6yr, ewe_age_over_6yr, lamb_sex_ram, lamb_sex_ewe, lamb_sex_wether, birth_and_rear_11, birth_and_rear_12, birth_and_rear_21, birth_and_rear_22, birth_and_rear_31, birth_and_rear_32, birth_and_rear_33, source_reference ) 
VALUES(2,'Targhee',1.14,1.08,1,1.02,0.91,1,0.97,1,1.17,1.12,1.23,1.2,1.31,1.36,'2015 Vol. 8 edition of the Sheep Production Manual page 52');
INSERT INTO sheep_weaning_adjustment_table(id_sheepweaningadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3_6yr, ewe_age_over_6yr, lamb_sex_ram, lamb_sex_ewe, lamb_sex_wether, birth_and_rear_11, birth_and_rear_12, birth_and_rear_21, birth_and_rear_22, birth_and_rear_31, birth_and_rear_32, birth_and_rear_33, source_reference ) 
VALUES(3,'Suffolk',1.14,1.05,1,1.06,0.91,1,0.97,1,1.14,1.11,1.19,1.17,1.29,1.38,'2015 Vol. 8 edition of the Sheep Production Manual page 52');
INSERT INTO sheep_weaning_adjustment_table(id_sheepweaningadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3_6yr, ewe_age_over_6yr, lamb_sex_ram, lamb_sex_ewe, lamb_sex_wether, birth_and_rear_11, birth_and_rear_12, birth_and_rear_21, birth_and_rear_22, birth_and_rear_31, birth_and_rear_32, birth_and_rear_33, source_reference ) 
VALUES(4,'Polypay',1,1,1,1.04,0.93,1,0.97,1,1.1,1.1,1.18,1.15,1.24,1.36,'2015 Vol. 8 edition of the Sheep Production Manual page 52');

```