# General Rules
Primary keys for all tables start with id_ followed by the table name without underscores and ending in id.

Dates are always text strings in the format YYYY-MM-DD
Times are always text string in the format HH:MM:SS
We do not handle time zones.
Where a date or time field is unknown it is replaced with leading zeros and then a 1 so we have no zero times. 00:00:01 for time 0000-00-01 for dates

In general I try to use the primary key in tables as a foreign key. Sometimes the name is changed for clarification but it's still a foreign key into another table.

NULL is not an allowed entry into any field. 

I realize that table and field names are very long. This is by design. I wanted to be sure that the casual reader understands what each table contains and how it is used. A bit of pain for the programmers now will save hours of "what is this table?" and "what is this field?"  questions in the future.

There are total of 101 basic tables in the AnimalTrakker system.

Insert statements to pre-fill the table with defaults  
Example:
```SQL
INSERT INTO birth_type_table(id_birthtypeid,birth_type,birth_type_abbrev,birth_type_display_order) VALUES (1,'Single','S',1);
```