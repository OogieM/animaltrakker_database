## death_reason_table
```SQL
CREATE TABLE "death_reason_table" 
	("id_deathreasonid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "death_reason" TEXT NOT NULL
	, "id_registry_id_contactsid" INTEGER NOT NULL
	, "death_reason_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (1,'Died Unknown',12,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (2,'Died Old Age',8,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (3,'Died Predator',10,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (4,'Died Illness',5,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (5,'Died Injury',6,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (6,'Died Stillborn',11,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (7,'Died Birth Defect',1,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (8,'Died Meat',7,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (9,'Died Euthanized',4,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (10,'Died Dystocia',3,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (11,'Died Other',9,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (12,'Died Culled',2,25);
INSERT INTO death_reason_table(id_deathreasonid,death_reason,death_reason_display_order,id_registry_id_contactsid) VALUES (13,'Died Administrative',13,25);

```