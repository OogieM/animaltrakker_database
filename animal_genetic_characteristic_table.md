## animal_genetic_characteristic_table
This will replace the Codon tables for sheep and is more generic to allow for other species genetic characteristics. This is the table that contains the results for individual animals.
```SQL
CREATE TABLE "animal_genetic_characteristic_table"
	("id_animalgeneticcharacteristicid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL
	, "id_geneticcharacteristictableid" INTEGER NOT NULL
	, "id_geneticcharacteristicvalueid" INTEGER NOT NULL
	, "id_geneticcharacteristiccalculationmethodid" INTEGER NOT NULL
	, "genetic_characteristic_date" TEXT NOT NULL
	, "genetic_characteristic_time" TEXT NOT NULL
	)
```