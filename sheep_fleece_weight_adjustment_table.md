## sheep_fleece_weight_adjustment_table
Used in sheep to adjust fleece weights for age of the ewe. 
```SQL
CREATE TABLE "sheep_fleece_weight_adjustment_table" 
	("id_sheepfleeceweightadjustmentid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "adjustment_name" TEXT NOT NULL
	, "ewe_age_2yr" REAL NOT NULL
	, "ewe_age_3yr" REAL NOT NULL
	, "ewe_age_4_5yr" REAL NOT NULL
	, "ewe_age_6_7yr" REAL NOT NULL
	, "ewe_age_8yr_up" REAL NOT NULL
	, "source_reference" TEXT NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO sheep_fleece_weight_adjustment_table (id_sheepfleeceweightadjustmentid, adjustment_name, ewe_age_2yr, ewe_age_3yr, ewe_age_4_5yr, ewe_age_6_7yr, ewe_age_8yr_up, source_reference)
VALUES (1, 'Generic', 1.07, 1.02, 1, 1.03, 1.05, '2015 Vol. 8 edition of the Sheep Production Manual page 52')

```