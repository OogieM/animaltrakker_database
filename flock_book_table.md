## flock_book_table
```SQL
CREATE TABLE "flock_book_table" 
	("id_flockbookid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_registry_id_contactsid" INTEGER NOT NUll
	, "flock_book_volume_name" TEXT NOT NUll
	, "flock_book_publication_date" TEXT NOT NUll
	, "flock_book_volume_display_order" INTEGER NOT NUll
	);
```