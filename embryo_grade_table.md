## embryo_grade_table
```SQL
CREATE TABLE "embryo_grade_table" 
	("id_embryogradeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "embryo_grade_name" TEXT NOT NULL UNIQUE 
	, "embryo_grade_display_order" INTEGER NOT NULL
	);
```