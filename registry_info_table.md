## registry_info_table
```SQL
CREATE TABLE "registry_info_table" 
	("id_registryinfoid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" INTEGER NOT NUll
	, "registry_founded_date" TEXT NOT NUll
	, "registry_closed_date" TEXT NOT NUll
	, "registry_abbrev" TEXT NOT NUll
	, "id_speciesid" INTEGER NOT NUll
	);
```