## farm_location_table
```SQL
CREATE TABLE "farm_location_table" 
	("id_farmlocationid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "farm_location_name" TEXT NOT NULL
	, "farm_location_abbreviation" TEXT NOT NULL
	, "id_contactsid" INTEGER NOT NULL
	, "farm_location_display_order" INTEGER NOT NULL
	);
```