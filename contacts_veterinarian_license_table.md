## contacts_veterinarian_license_table
```SQL
CREATE TABLE "contacts_veterinarian_license_table" 
	("id_contactsveterinarianlicenseid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" TEXT NOT NUll
	, "vet_license_type" INTEGER NOT NULL
	, "vet_license_number" TEXT NOT NUll
	, "vet_license_state_submission_status" TEXT NOT NULL
	, "vet_license_state" INTEGER NOT NULL
	, "vet_license_expiration_date" TEXT NOT NULL
	);
```