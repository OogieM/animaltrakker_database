## coat_color_table
```SQL
CREATE TABLE "coat_color_table" 
	("id_coatcolorid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "coat_color" TEXT
	, "coat_color_abbrev" TEXT
	, "id_registry_id_contactsid" INTEGER
	, "coat_color_display_order" INTEGER
	)
```
