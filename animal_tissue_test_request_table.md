## animal_tissue_test_request_table
```SQL
CREATE TABLE "animal_tissue_test_request_table"
	( "id_animaltissuetestrequestid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animaltissuesampletakenid" INTEGER NOT NUll
	, "id_tissuetestid" INTEGER NOT NUll
	, "id_lab_id_contactsid" INTEGER NOT NUll
	, "tissue_sample_lab_accession_id" TEXT NOT NUll
	, "tissue_test_results" TEXT NOT NUll
	, "tissue_test_results_date" TEXT NOT NUll
	, "tissue_test_results_time" TEXT NOT NUll
	, "id_tissue_test_results_id_animalexternalfileid" INTEGER NOT NUll
	);
```