## scrapie_flock_table
Used for US sheep and goats for the official federal scrapie flock id number. 
```SQL
CREATE TABLE "scrapie_flock_table" 
	("id_scrapieflockid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "scrapie_flockid" TEXT NOT NULL
	, "id_contactsid" INTEGER NOT NULL
	, "scrapie_flock_display_order" INTEGER NOT NULL
	);
```

