## sheep_lambs_born_adjustment_table
Used for sheep to adjust number of lambs born to account for breed and age of ewe. 
```SQL
CREATE TABLE "sheep_lambs_born_adjustment_table" 
	("id_sheeplambsbornadjustmentid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "adjustment_name" TEXT NOT NULL
	, "ewe_age_1yr" REAL NOT NULL
	, "ewe_age_2yr" REAL NOT NULL
	, "ewe_age_3yr" REAL NOT NULL
	, "ewe_age_4yr" REAL NOT NULL
	, "ewe_age_5yr" REAL NOT NULL
	, "ewe_age_6yr" REAL NOT NULL
	, "ewe_age_7yr" REAL NOT NULL
	, "ewe_age_8yr" REAL NOT NULL
	, "ewe_age_9yr_up" REAL NOT NULL
	, "source_reference" TEXT NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO sheep_lambs_born_adjustment_table (id_sheeplambsbornadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3yr, ewe_age_4yr, ewe_age_5yr, ewe_age_6yr, ewe_age_7yr, ewe_age_8yr, ewe_age_9yr_up, source_reference )
VALUES (1,'Generic',1.48,1.17,1.05,1.01,1,1,1.02,1.05,1.13,'2015 Vol. 8 edition of the Sheep Production Manual page 52');
INSERT INTO sheep_lambs_born_adjustment_table (id_sheeplambsbornadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3yr, ewe_age_4yr, ewe_age_5yr, ewe_age_6yr, ewe_age_7yr, ewe_age_8yr, ewe_age_9yr_up, source_reference )
VALUES (2,'Targhee',1.57,1.24,1.1,1.03,1,0.99,1.03,1.03,1.13,'2015 Vol. 8 edition of the Sheep Production Manual page 52');
INSERT INTO sheep_lambs_born_adjustment_table (id_sheeplambsbornadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3yr, ewe_age_4yr, ewe_age_5yr, ewe_age_6yr, ewe_age_7yr, ewe_age_8yr, ewe_age_9yr_up, source_reference )
VALUES (3,'Suffolk',1.34,1.08,1.01,1,1,1,1,1.03,1.12,'2015 Vol. 8 edition of the Sheep Production Manual page 52');
INSERT INTO sheep_lambs_born_adjustment_table (id_sheeplambsbornadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3yr, ewe_age_4yr, ewe_age_5yr, ewe_age_6yr, ewe_age_7yr, ewe_age_8yr, ewe_age_9yr_up, source_reference )
VALUES (4,'PolyPay',1.52,1.2,1.05,1,1,1.02,1.02,1.08,1.13,'2015 Vol. 8 edition of the Sheep Production Manual page 52');

```