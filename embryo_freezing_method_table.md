## embryo_freezing_method_table
```SQL
CREATE TABLE "embryo_freezing_method_table" 
	("id_embryofreezingmethodid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "embryo_freezing_method_name" TEXT NOT NULL UNIQUE
	, "embryo_freezing_method_display_order" INTEGER NOT NULL
	);
```