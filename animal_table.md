## animal_table
Per veterinarian and rancher inputs Where exact birthdates are not known we impute a birth date. For age and month data inputs the birth date is presumed to be the first day of that month. For year only inputs the birth date is assumes to be 01 January of that year. This is the standard practice for animal births. 

Obviously pedigree breeders are more likely to have more accurate birthdates but we have to account for the commercial folks as well. 

```SQL
--	The sire_id and dam_id are pointers into this table
-- 	id_breeder_id_contactsid is a pointer into the contacts table of the person and location that is the breeder. 
CREATE TABLE "animal_table" 
	("id_animalid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "animal_name" TEXT NOT NULL
	, "id_sexid" INTEGER NOT NULL
	, "birth_date" TEXT NOT NULL
	, "birth_time" TEXT NOT NULL
	, "id_birthtypeid" INTEGER NOT NULL
	, "birth_weight" REAL NOT NULL
	, "birth_weight_id_unitsid" INTEGER NOT NULL
	, "id_birthinghistoryid" INTEGER NOT NULL
	, "rear_type" INTEGER NOT NULL
	, "weaned_date" TEXT NOT NULL
	, "death_date" TEXT NOT NULL
	, "id_deathreasonid" INTEGER NOT NULL
	, "sire_id" INTEGER NOT NULL
	, "dam_id" INTEGER NOT NULL
	, "inbreeding" REAL NOT NULL
	, "id_breeder_id_contactsid" INTEGER NOT NULL
	, "id_managementgroupid" INTEGER NOT NULL
	, "alert" TEXT NOT NULL
	)
```