## animaltrakker_default_settings
```SQL
--	Most of these are pointers into the referenced table
--  All the color fields are pointers into the tag_color_table
--	All the location fields are pointers into the id_location_table
--  All the contact fields are pointers into the contacts_table
CREATE TABLE "animaltrakker_default_settings_table" 
	("id_animaltrakkerdefaultsettingsid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "owner_id_contactsid" INTEGER NOT NULL
	, "vet_id_contactsid" INTEGER NOT NULL
	, "lab_id_contactsid" INTEGER NOT NULL
	, "id_registry_id_contactsid" INTEGER NOT NULL
	, "id_contactsstateid" INTEGER NOT NULL
	, "id_flockprefixid" INTEGER NOT NULL	
	, "id_speciesid" INTEGER NOT NULL
	, "id_breedid" INTEGER NOT NULL
	, "id_sexid" INTEGER NOT NULL	
	, "early_gestation_length" INTEGER NOT NULL
	, "late_gestation_length" INTEGER NOT NULL
	, "typical_gestation_length" INTEGER NOT NULL
	, "id_idtypeid" INTEGER NOT NULL 
	, "id_eid_tag_male_color_female_color_same" INTEGER NOT NULL
	, "eid_tag_color_male" INTEGER NOT NULL
	, "eid_tag_color_female" INTEGER NOT NULL
	, "eid_tag_location" INTEGER NOT NULL	
	, "id_farm_tag_male_color_female_color_same" INTEGER NOT NULL
	, "farm_tag_based_on_eid_tag" INTEGER NOT NULL
	, "farm_tag_number_digits_from_eid" INTEGER NOT NULL
	, "farm_tag_color_male" INTEGER NOT NULL
	, "farm_tag_color_female" INTEGER NOT NULL
	, "farm_tag_location" INTEGER NOT NULL 
	, "id_fed_tag_male_color_female_color_same" INTEGER NOT NULL
	, "fed_tag_color_male" INTEGER NOT NULL
	, "fed_tag_color_female" INTEGER NOT NULL
	, "fed_tag_location" INTEGER NOT NULL 	
	, "id_nues_tag_male_color_female_color_same" INTEGER NOT NULL
	, "nues_tag_color_male" INTEGER NOT NULL
	, "nues_tag_color_female" INTEGER NOT NULL
	, "nues_tag_location" INTEGER NOT NULL 	 
	, "id_trich_tag_male_color_female_color_same" INTEGER NOT NULL
	, "trich_tag_color_male" INTEGER NOT NULL
	, "trich_tag_color_female" INTEGER NOT NULL
	, "trich_tag_location" INTEGER NOT NULL 
	, "trich_tag_auto_increment" INTEGER NOT NULL
	, "trich_tag_next_tag_number" INTEGER NOT NULL
	, "use_paint_marks" INTEGER NOT NULL
	, "paint_tag_color" INTEGER NOT NULL
	, "paint_tag_location" INTEGER NOT NULL 	
	, "id_idremovereasonid" INTEGER NOT NULL	
	, "tattoo_color" INTEGER NOT NULL
	, "tattoo_location" INTEGER NOT NULL 	 
	, "freeze_brand_location" INTEGER NOT NULL 	 
	, "id_tissuesampletypeid" INTEGER NOT NULL
	, "id_tissuetestid" INTEGER NOT NULL
	, "id_tissuesamplecontainertypeid" INTEGER NOT NULL	
	, "evaluation_update_alert" INTEGER NOT NULL
	, "weight_id_unitsid" INTEGER NOT NULL
	, "birth_type" INTEGER NOT NULL
	, "birth_weight_id_unitsid" INTEGER NOT NULL
	, "rear_type" INTEGER NOT NULL	 
	, "sale_price_id_unitsid" INTEGER NOT NULL
	, "bangs_tag_color_male" INTEGER NOT NULL
	, "bangs_tag_color_female" INTEGER NOT NULL
	, "bangs_tag_location" INTEGER NOT NULL 
	, "id_contactscountyid" INTEGER NOT NULL
	, "breeder_id_contactsid" INTEGER NOT NULL
	, "id_deathreasonid" INTEGER NOT NULL
	, "id_transferreasonid" INTEGER NOT NULL
	, "user_system_serial_number" INTEGER NOT NULL
	);
```