## flock_prefix_table
This can also be a herd prefix. Since AnimalTrakker started as a sheep program I left the table name flock for convenience. 
```SQL
--	id_contactsid is for the registry that this flock prefix applies to
CREATE TABLE "flock_prefix_table" 
	("id_flockprefixid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "flock_prefix" TEXT NOT NULL 
	, "id_contactsid" INTEGER NOT NUll
	);
```
