## contacts_title_table
```SQL
CREATE TABLE "contacts_title_table" 
	("id_contactstitleid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "contacts_title" TEXT NOT NULL
	, "contacts_title_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO contacts_title_table(id_contactstitleid,contacts_title,contacts_title_display_order) VALUES (1,'Mr.',3);
INSERT INTO contacts_title_table(id_contactstitleid,contacts_title,contacts_title_display_order) VALUES (2,'Ms.',1);
INSERT INTO contacts_title_table(id_contactstitleid,contacts_title,contacts_title_display_order) VALUES (3,'Mrs.',2);
INSERT INTO contacts_title_table(id_contactstitleid,contacts_title,contacts_title_display_order) VALUES (4,'Mr. and Mrs.',4);
INSERT INTO contacts_title_table(id_contactstitleid,contacts_title,contacts_title_display_order) VALUES (5,'Dr.',5);
INSERT INTO contacts_title_table(id_contactstitleid,contacts_title,contacts_title_display_order) VALUES (6,'Unknown',6);
```