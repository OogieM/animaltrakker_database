## contacts_country_table
```SQL
CREATE TABLE "contacts_country_table" 
	("id_contactscountryid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "country_name" TEXT NOT NULL
	, "country_abbrev" TEXT NOT NULL
	, "country_name_display_order" INTEGER NOT NULL 
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO contacts_country_table(id_contactscountryid,country_name,country_abbrev,country_name_display_order) VALUES (1,'United States','US',1);
INSERT INTO contacts_country_table(id_contactscountryid,country_name,country_abbrev,country_name_display_order) VALUES (2,'Canada','Can',2);
INSERT INTO contacts_country_table(id_contactscountryid,country_name,country_abbrev,country_name_display_order) VALUES (3,'United Kingdom','UK',3);
```