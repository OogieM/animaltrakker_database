## drug_type_table
```SQL
CREATE TABLE "drug_type_table" 
	("id_drugtypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "drug_type" TEXT NOT NULL
	, "drug_type_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO drug_type_table(id_drugtypeid,drug_type,drug_type_display_order) VALUES (1,'Dewormer',7);
INSERT INTO drug_type_table(id_drugtypeid,drug_type,drug_type_display_order) VALUES (2,'Vaccine',6);
INSERT INTO drug_type_table(id_drugtypeid,drug_type,drug_type_display_order) VALUES (3,'Antibiotic',2);
INSERT INTO drug_type_table(id_drugtypeid,drug_type,drug_type_display_order) VALUES (4,'Hormone',5);
INSERT INTO drug_type_table(id_drugtypeid,drug_type,drug_type_display_order) VALUES (5,'Coccidiostat',3);
INSERT INTO drug_type_table(id_drugtypeid,drug_type,drug_type_display_order) VALUES (6,'Feed Supplement',4);
INSERT INTO drug_type_table(id_drugtypeid,drug_type,drug_type_display_order) VALUES (7,'Analgesic',1);

```