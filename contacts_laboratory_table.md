## contacts_laboratory_table
```SQL
CREATE TABLE "contacts_laboratory_table" 
	("id_contactslaboratoryid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" TEXT NOT NUll
	, "laboratory_display_order" INTEGER NOT NUll
	, "lab_license_number" INTEGER NOT NUll
	);
```