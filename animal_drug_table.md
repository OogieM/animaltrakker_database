## animal_drug_table
```SQL
CREATE TABLE "animal_drug_table"
	("id_sheepdrugid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL
	, "id_drugid" INTEGER NOT NULL
	, "drug_date_on" TEXT NOT NULL
	, "drug_time_on" TEXT NOT NULL
	, "drug_date_off" TEXT NOT NULL
	, "drug_time_off" TEXT NOT NULL
	, "drug_dosage" TEXT NOT NULL
	, "id_druglocationid" INTEGER NOT NULL
	)
```