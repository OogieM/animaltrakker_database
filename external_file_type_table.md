## external_file_type_table
```SQL
CREATE TABLE "external_file_type_table" 
	("id_externalfiletypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "external_file_type" TEXT NOT NULL
	, "external_file_type_suffix" TEXT NOT NULL
	, "external_file_type_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults

```SQL
INSERT INTO external_file_type_table(id_externalfiletypeid,external_file_type_display_order,external_file_type,external_file_type_suffix) VALUES (1,1,'Picture-JPG','JPG');
INSERT INTO external_file_type_table(id_externalfiletypeid,external_file_type_display_order,external_file_type,external_file_type_suffix) VALUES (2,2,'FLIR-JPG','JPG');
INSERT INTO external_file_type_table(id_externalfiletypeid,external_file_type_display_order,external_file_type,external_file_type_suffix) VALUES (3,3,'Portable Document','PDF');
INSERT INTO external_file_type_table(id_externalfiletypeid,external_file_type_display_order,external_file_type,external_file_type_suffix) VALUES (4,4,'DNA Sequence-FASTQ','fastq.gz');
INSERT INTO external_file_type_table(id_externalfiletypeid,external_file_type_display_order,external_file_type,external_file_type_suffix) VALUES (5,5,'CSV','CSV');

```