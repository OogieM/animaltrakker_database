## birth_type_table
```SQL
CREATE TABLE "birth_type_table" 
	( "id_birthtypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "birth_type" TEXT NOT NULL
	, "birth_type_abbrev" TEXT NOT NULL
	, "birth_type_display_order" INTEGER NOT NULL
	)
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO birth_type_table(id_birthtypeid,birth_type,birth_type_abbrev,birth_type_display_order) VALUES (1,'Single','S',1);
INSERT INTO birth_type_table(id_birthtypeid,birth_type,birth_type_abbrev,birth_type_display_order) VALUES (2,'Twin','T',2);
INSERT INTO birth_type_table(id_birthtypeid,birth_type,birth_type_abbrev,birth_type_display_order) VALUES (3,'Triplet','Tr',3);
INSERT INTO birth_type_table(id_birthtypeid,birth_type,birth_type_abbrev,birth_type_display_order) VALUES (4,'Quadruplet','4',4);
INSERT INTO birth_type_table(id_birthtypeid,birth_type,birth_type_abbrev,birth_type_display_order) VALUES (5,'Quintuplet','5',5);
INSERT INTO birth_type_table(id_birthtypeid,birth_type,birth_type_abbrev,birth_type_display_order) VALUES (6,'Sextuplet','6',6);
INSERT INTO birth_type_table(id_birthtypeid,birth_type,birth_type_abbrev,birth_type_display_order) VALUES (42,'Unknown','U',42);
```