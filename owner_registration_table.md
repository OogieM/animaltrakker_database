## owner_registration_table
```SQL
CREATE TABLE "owner_registration_table" 
	("id_ownerregistrationid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" INTEGER NOT NULL 
	, "membership_number" TEXT NOT NULL
	, "id_registry_id_contactsid" INTEGER NOT NULL 
	, "date_joined" TEXT NOT NUll
	, "dues_paid_until" TEXT NOT NUll
	, "date_resigned" TEXT NOT NUll
	, "online_password" TEXT NOT NUll
	, "id_membershipregionid" INTEGER NOT NUll
	, "id_flockprefixid" INTEGER NOT NUll
	, "id_registrymembershipstatusid" INTEGER NOT NUll
	, "id_registrymembershiptypeid" INTEGER NOT NUll
	, "board_member" INTEGER NOT NUll
	, "last_census" TEXT NOT NUll
	);
```