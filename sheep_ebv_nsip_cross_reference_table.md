## sheep_ebv_nsip_cross_reference_table
```SQL
CREATE TABLE "sheep_ebv_nsip_cross_reference_table" 
	("id_sheepebvnsipcrossreferenceid" INTEGER PRIMARY KEY NOT NULL
	, "animaltrakker_table_name" TEXT NOT NULL
	, "animaltrakker_field_name" TEXT NOT NULL
	, "lambplan_field_name" TEXT NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_table','nsip_id','ID');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_table','sex','SEX');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_date','DATE');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','which_run','RUN_NAME');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_birth_weight','BWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_birth_weight_acc','ACC_BWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_wean_weight','WWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_wean_weight_acc','ACC_WWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_weight','PWWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_weight_acc','ACC_PWWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_weight','YWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_weight_acc','ACC_YWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_weight','HWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_weight_acc','ACC_HWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_weight','AWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_weight_acc','ACC_AWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_wean_fat','WFAT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_wean_fat_acc','ACC_WCF');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_fat','PFAT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_fat_acc','ACC_PWCF');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fat','YFAT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fat_acc','ACC_YCF');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fat','HFAT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fat_acc','ACC_HCF');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_wean_emd','WEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_wean_emd_acc','ACC_WEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_emd','PEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_emd_acc','ACC_PEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_emd','YEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_emd_acc','ACC_YEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_emd','HEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_emd_acc','ACC_HEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fleece_diameter','YFD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fleece_diameter_acc','ACC_YFD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fleece_diameter','HFD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fleece_diameter_acc','ACC_HFD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_fleece_diameter','AFD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_fleece_diameter_acc','ACC_AFD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_greasy_fleece_weight','YGFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_greasy_fleece_weight_acc','ACC_YGFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_greasy_fleece_weight','HGFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_greasy_fleece_weight_acc','ACC_HGFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_greasy_fleece_weight','AGFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_greasy_fleece_weight_acc','ACC_AGFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_clean_fleece_weight','YCFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_clean_fleece_weight_acc','ACC_YCFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_clean_fleece_weight','HCFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_clean_fleece_weight_acc','ACC_HCFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_clean_fleece_weight','ACFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_clean_fleece_weight_acc','ACC_ACFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fleece_yield','YYLD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fleece_yield_acc','ACC_YYLD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fleece_yield','HYLD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fleece_yield_acc','ACC_HYLD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_fleece_yield','AYLD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_fleece_yield_acc','ACC_AYLD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fiber_diameter_variation','YFDCV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fiber_diameter_variation_acc','ACC_YFDCV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fiber_diameter_variation','HFDCV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fiber_diameter_variation_acc','ACC_HFDCV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_fiber_diameter_variation','AFDCV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_fiber_diameter_variation_acc','ACC_AFDCV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_staple_strength','YSS');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_staple_strength_acc','ACC_YSS');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_staple_strength','HSS');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_staple_strength_acc','ACC_HSS');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_staple_strength','ASS');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_staple_strength_acc','ACC_ASS');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_staple_length','YSL');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_staple_length_acc','ACC_YSL');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_staple_length','HSL');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_staple_length_acc','ACC_HSL');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_staple_length','ASL');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_staple_length_acc','ACC_ASL');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fleece_curvature','YCURV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fleece_curvature_acc','ACC_YCURV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fleece_curvature','HCURV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fleece_curvature_acc','ACC_HCURV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_fleece_curvature','ACURV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_adult_fleece_curvature_acc','ACC_ACURV');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_wean_fec','WFEC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_wean_fec_acc','ACC_WFEC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_fec','PFEC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_fec_acc','ACC_PFEC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fec','YFEC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_fec_acc','ACC_YFEC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fec','HFEC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_fec_acc','ACC_HFEC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_scrotal','PSC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_post_wean_scrotal_acc','ACC_PSC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_scrotal','YSC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_scrotal_acc','ACC_YSC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_scrotal','HSC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_hogget_scrotal_acc','ACC_HSC');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_number_lambs_born','NLB');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_number_lambs_born_acc','ACC_NLB');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_number_lambs_born','YNLB');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_number_lambs_born_acc','ACC_YNLB');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_number_lambs_weaned','NLW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_number_lambs_weaned_acc','ACC_NLW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_number_lambs_weaned','YNLW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_yearling_number_lambs_weaned_acc','ACC_YNLW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_maternal_birth_weight','MBWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_maternal_birth_weight_acc','ACC_MBWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_maternal_wean_weight','MWWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_maternal_wean_weight_acc','ACC_MLWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','border_dollar_index','INDEX_2');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','border_dollar_acc','A_INDEX_2');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','self_replacing_carcass_index','INDEX_4');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','self_replacing_carcass_acc','A_INDEX_4');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','self_replacing_carcass_no_repro_index','INDEX04');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','self_replacing_carcass_no_repro_acc','A_INDEX04');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','carcass_plus_index','INDEX_8');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','carcass_plus_acc','A_INDEX_8');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','spare_index','INDEX_9');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','spare_index_acc','A_INDEX_9');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','lamb_2020_index','INDEX_13');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','lamb_2020__acc','A_INDEX_13');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','maternal_dollar_index','INDEX_14');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','maternal_dollar_acc','A_INDEX_14');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','maternal_dollar_no_repro_index','INDEX014');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','maternal_dollar_no_repro_acc','A_INDEX014');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','coopworth_dollar_index','INDEX_15');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','coopworth_dollar_acc','A_INDEX_15');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','spare_2_index','INDEX_24');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','spare_2_index_acc','A_INDEX_24');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','dual_purpose_dollar_index','INDEX_25');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','dual_purpose_dollar_acc','A_INDEX_25');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','dual_purpose_dollar_no_repro_index','INDEX025');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','dual_purpose_dollar_no_repro_acc','A_INDEX025');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','export_index','INDEX_E');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','trade_index','INDEX_T');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','merino_dp_index','INDEX1');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','merino_dp_acc','A_INDEX1');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','merino_dp_plus_index','INDEX2');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','merino_dp_plus_acc','A_INDEX2');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','fine_medium_index','INDEX3');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','fine_medium_acc','A_INDEX3');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','fine_medium_plus_index','INDEX4');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','fine_medium_plus_acc','A_INDEX4');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','samm_index','INDEX5');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','samm_acc','A_INDEX5');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','dohne_no_repro_index','INDEX6');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','dohne_no_repro_acc','A_INDEX6');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','superfine_index','INDEX7');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','superfine_acc','A_INDEX7');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','superfine_plus_index','INDEX8');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','superfine_plus_acc','A_INDEX8');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','usa_maternal_index','USERINDEX');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','maternal_greasy_fleece_weight','MGFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','maternal_greasy_fleece_weight_acc','ACC_MGFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','maternal_clean_fleece_weight','MCFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','maternal_clean_fleece_weight_acc','ACC_MCFW');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_lambease_direct','LE_DIR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_lambease_direct_acc','ACC_LE_DIR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_lambease_daughter','LE_DAU');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_lambease_daughter_acc','ACC_LE_DAU');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_gestation_length','GL_DIR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_gestation_length_acc','ACC_GL_DIR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_gestation_length_daughter','GL_DAU');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','ebv_gestation_length_daughter_acc','ACC_GL_DAU');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','early_breech_wrinkle','EBWR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','early_breech_wrinkle_acc','ACC_EBWR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','late_breech_wrinkle','LBWR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','late_breech_wrinkle_acc','ACC_LBWR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','early_body_wrinkle','EDBWR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','early_body_wrinkle_acc','ACC_EDBWR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','late_body_wrinkle','LDBWR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','late_body_wrinkle_acc','ACC_LDBWR');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','late_dag','LDAG');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','late_dag_acc','ACC_LDAG');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','intramuscular_fat','IMF');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','intramuscular_fat_acc','ACC_IMF');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','carcass_shear_force','SHRF5');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','carcass_shear_force_acc','ACC_SHRF5');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','dressing_percentage','DRESS');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','dressing_percentage_acc','ACC_DRESS');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','lean_meat_yield','LMY');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','lean_meat_yield_acc','ACC_LMY');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','hot_carcass_weight','HCWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','hot_carcass_weight_acc','ACC_HCWT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','carcass_fat','CCFAT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','carcass_fat_acc','ACC_CCFAT');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','carcass_eye_muscle_depth','CCEMD');
INSERT INTO sheep_ebv_nsip_cross_reference_table (animaltrakker_table_name,animaltrakker_field_name,lambplan_field_name)
VALUES('sheep_ebv_table','carcass_eye_muscle_depth_acc','ACC_CCEMD');

```