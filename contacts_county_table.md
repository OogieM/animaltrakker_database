## contacts_county_table
```SQL
CREATE TABLE "contacts_county_table" 
	("id_contactscountyid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "county_name" TEXT NOT NULL
	, "county_abbrev" TEXT NOT NULL
	, "id_contactsstateid"  INTEGER NOT NULL
	, "contacts_county_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (1,'Adams','AD',6,1);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (2,'Alamosa','AL',6,2);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (3,'Arapahoe','AR',6,3);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (4,'Archuleta','AU',6,4);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (5,'Baca','BC',6,5);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (6,'Bent','BN',6,6);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (7,'Boulder','BL',6,7);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (8,'Broomfield','BR',6,8);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (9,'Chaffee','CF',6,9);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (10,'Cheyenne','CY',6,10);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (11,'Clear Creek','CC',6,11);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (12,'Conejos','CN',6,12);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (13,'Costilla','CS',6,13);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (14,'Crowley','CR',6,14);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (15,'Custer','CT',6,15);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (16,'Delta','DL',6,16);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (17,'Denver','DN',6,17);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (18,'Dolores','DO',6,18);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (19,'Douglas','DG',6,19);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (20,'Eagle','EG',6,20);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (21,'El Paso','EP',6,21);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (22,'Elbert','EL',6,22);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (23,'Fremont','FR',6,23);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (24,'Garfield','GF',6,24);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (25,'Gilpin','GP',6,25);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (26,'Grand','GR',6,26);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (27,'Gunnison','GU',6,27);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (28,'Hinsdale','HN',6,28);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (29,'Huerfano','HF',6,29);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (30,'Jackson','JL',6,30);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (31,'Jefferson','JF',6,31);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (32,'Kiowa','KI',6,32);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (33,'Kit Carson','KC',6,33);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (34,'La Plata','LP',6,40);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (35,'Lake','LK',6,35);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (36,'Larimer','LR',6,36);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (37,'Las Animas','LA',6,37);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (38,'Lincoln','LN',6,38);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (39,'Logan','LO',6,39);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (40,'Mesa','ME',6,40);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (41,'Mineral','MN',6,41);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES (42,'Moffat','MO',6,42);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(43,'Montezuma','MZ',6,43);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(44,'Montrose','MO',6,44);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(45,'Morgan','MR',6,45);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(46,'Otero','OT',6,46);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(47,'Ouray','OU',6,47);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(48,'Park','PK',6,48);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(49,'Phillips','PH',6,49);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(50,'Pitkin','PT',6,50);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(51,'Prowers','PR',6,51);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(52,'Pueblo','PB',6,52);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(53,'Rio Blanco','RB',6,53);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(54,'Rio Grande','RG',6,54);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(55,'Routt','RT',6,55);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(56,'Saguache','SA',6,56);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(57,'San Juan','SJ',6,57);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(58,'San Miguel','SM',6,58);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(59,'Sedgwick','SE',6,59);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(60,'Summit','ST',6,60);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(61,'Teller','TL',6,61);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(62,'Washington','WA',6,62);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(63,'Weld','WE',6,63);
INSERT INTO contacts_county_table(id_contactscountyid,county_name,county_abbrev,id_contactsstateid,contacts_county_display_order) VALUES(64,'Yuma','YU',6,64);
 
 ```