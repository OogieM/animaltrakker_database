## tissue_sample_type_table
```SQL
CREATE TABLE "tissue_sample_type_table"
	( "id_tissuesampletypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "tissue_sample_type_name" TEXT NOT NULL
	, "tissue_sample_type_abbrev" TEXT NOT NULL
	, "tissue_sample_type_display_order" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_tissue_sample_type_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (1,1,'Blood','Bl');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (2,2,'Ear Punch','Ear');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (3,3,'Hair','Hair');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (4,4,'Feces','Fec');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (5,5,'Milk','M');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (6,6,'Urine','Ur');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (7,7,'Lung','L');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (8,8,'Liver','Liv');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (9,9,'Lymph','Lym');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (10,10,'Intestine','Int');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (11,11,'Kidney','Kid');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (12,12,'Brain','Br');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (13,13,'Fetus','Fet');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (14,14,'Whole Animal','Whole Animal');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (15,15,'Preputial Scraping','Preputial');
INSERT INTO tissue_sample_type_table(id_tissuesampletypeid,tissue_sample_type_display_order,tissue_sample_type_name,tissue_sample_type_abbrev) VALUES (16,15,'Semen','Sem');

```