## registry_membership_region_table
```SQL
--	id_contactsid is for the registry that this region applies to
CREATE TABLE "registry_membership_region_table" 
	("id_registrymembershipregionid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" INTEGER NOT NUll
	, "membership_region" TEXT NOT NUll
	, "membership_region_number" INTEGER NOT NUll
	, "membership_region_display_order" INTEGER NOT NUll
	);
```

Insert statements to pre-fill the table with defaults.
```SQL
INSERT INTO registry_membership_region_table(id_registrymembershipregionid,id_contactsid,membership_region,membership_region_number, membership_region_display_order) 
VALUES (1,1,'Eastern',1,1);
INSERT INTO registry_membership_region_table(id_registrymembershipregionid,id_contactsid,membership_region,membership_region_number, membership_region_display_order) 
VALUES (2,1,'Central',2,2);
INSERT INTO registry_membership_region_table(id_registrymembershipregionid,id_contactsid,membership_region,membership_region_number, membership_region_display_order) 
VALUES (3,1,'Mountain',3,3);
INSERT INTO registry_membership_region_table(id_registrymembershipregionid,id_contactsid,membership_region,membership_region_number, membership_region_display_order) 
VALUES (4,1,'Pacific',4,4);
INSERT INTO registry_membership_region_table(id_registrymembershipregionid,id_contactsid,membership_region,membership_region_number, membership_region_display_order) 
VALUES (5,1,'Alaskan',5,5);
INSERT INTO registry_membership_region_table(id_registrymembershipregionid,id_contactsid,membership_region,membership_region_number, membership_region_display_order) 
VALUES (6,1,'Hawaii-Aleutian',6,6);
INSERT INTO registry_membership_region_table(id_registrymembershipregionid,id_contactsid,membership_region,membership_region_number, membership_region_display_order) 
VALUES (7,1,'Canada',7,7);
INSERT INTO registry_membership_region_table(id_registrymembershipregionid,id_contactsid,membership_region,membership_region_number, membership_region_display_order) 
VALUES (8,1,'Overseas',8,8);
```