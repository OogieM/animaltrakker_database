## animal_registration_table
```SQL
CREATE TABLE "animal_registration_table" 
	("id_animalregistrationid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NUll
	, "registration_number" TEXT NOT NUll
	, "registry_id_contactsid" INTEGER NOT NUll
	, "id_animalregistrationtypeid" INTEGER NOT NUll
	, "id_flockbookid" INTEGER NOT NUll
	, "registration_date" TEXT NOT NUll
	, "registration_description" TEXT NOT NUll
	)
```