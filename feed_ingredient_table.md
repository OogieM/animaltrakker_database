## feed_ingredient_table
```SQL
CREATE TABLE "feed_ingredient_table" 
	("id_feedingredientid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	,"feed_ingredient_name" TEXT NOT NULL 
	,"feed_ingredient_contactsid" INTEGER NOT NULL
	,"feed_ingredient_cost" REAL NOT NULL
	,"feed_ingredient_cost_id_unitsid" INTEGER NOT NULL
	,"feed_ingredient_amount_purchased" INTEGER NOT NULL
	,"feed_ingredient_amount_id_unitsid" INTEGER NOT NULL
	,"feed_purchase_date" TEXT NOT NULL
	,"feed_remove_date" TEXT NOT NULL
	,"feed_gone" INTEGER NOT NULL
	);
```