## tag_color_table
```SQL
CREATE TABLE "tag_color_table" 
	( "id_tagcolorid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "tag_color_name" TEXT NOT NULL
	, "tag_color_abbrev" TEXT NOT NULL
	, "tag_color_display_order" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_tag_colors_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (1,'Y','Yellow',2);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (2,'PR','Purple',9);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (3,'W','White',1);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (4,'MI','Mint',8);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (5,'O','Orange',3);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (6,'P','Pink',4);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (7,'BR','Brown',10);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (8,'R','Red',5);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (9,'G','Green',6);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (10,'B','Blue',7);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (11,'BK','Black',12);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (12,'GY','Grey',11);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (13,'M','Metal',13);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (14,'U','Unknown',14);
INSERT INTO tag_color_table(id_tagcolorid,tag_color_abbrev,tag_color_name,tag_color_display_order) VALUES (15,'NA','Not Applicable',15);

```