## management_group_table
```SQL
CREATE TABLE "management_group_table" 
	("id_managementgroupid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "management_group" TEXT NOT NULL
	, "management_group_display_order" INTEGER NOT NULL
	, "id_contactsid" INTEGER NOT NULL
	);
```