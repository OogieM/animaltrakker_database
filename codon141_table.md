## codon141_table
```SQL
CREATE TABLE "codon141_table" 
	("id_codon141id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "codon141_alleles" TEXT NOT NULL
	, "codon141_display_order" INTEGER NOT NULL
	)
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO
codon141_table (id_codon141id, codon141_alleles, codon141_display_order)
VALUES (1,'??',1);
INSERT INTO
codon141_table (id_codon141id, codon141_alleles, codon141_display_order)
VALUES (2,'FF',2);
INSERT INTO
codon141_table (id_codon141id, codon141_alleles, codon141_display_order)
VALUES (3,'F?',3);
INSERT INTO
codon141_table (id_codon141id, codon141_alleles, codon141_display_order)
VALUES (4,'FL',4);
INSERT INTO
codon141_table (id_codon141id, codon141_alleles, codon141_display_order)
VALUES (5,'L?',5);
INSERT INTO
codon141_table (id_codon141id, codon141_alleles, codon141_display_order)
VALUES (6,'LL',6);
```