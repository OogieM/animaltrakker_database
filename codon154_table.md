## codon154_table
```SQL
CREATE TABLE "codon154_table" 
	("id_codon154id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	,"codon154_alleles" TEXT NOT NULL
	,"codon154_display_order" INTEGER NOT NULL
	)
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO
codon154_table (id_codon154id, codon154_alleles, codon154_display_order)
VALUES (1,'??',1);
INSERT INTO
codon154_table (id_codon154id, codon154_alleles, codon154_display_order)
VALUES (2,'RR',2);
INSERT INTO
codon154_table (id_codon154id, codon154_alleles, codon154_display_order)
VALUES (3,'RH',3);
INSERT INTO
codon154_table (id_codon154id, codon154_alleles, codon154_display_order)
VALUES (4,'R?',4);
INSERT INTO
codon154_table (id_codon154id, codon154_alleles, codon154_display_order)
VALUES (5,'HH',5);
INSERT INTO
codon154_table (id_codon154id, codon154_alleles, codon154_display_order)
VALUES (6,'H?',6);
```