## registration_type_table
```SQL
--	id_contactsid is for the registry that this registration type applies to
CREATE TABLE "registration_type_table" 
	("id_registrationtypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" INTEGER NOT NUll
	, "registration_type" TEXT NOT NUll
	, "registration_type_abbrev" TEXT NOT NUll
	, "registration_type_display_order" INTEGER NOT NUll
	);
```