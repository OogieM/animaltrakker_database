## genetic_characteristic_table
This table contains the names of the different genetic characteristic tables.
```SQL
CREATE TABLE "genetic_characteristic_table"
	("id_geneticcharacteristicid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "genetic_characteristic_table_name" TEXT NOT NULL
	, "genetic_characteristic_table_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_order)
VALUES (1, 'codon136_table', 1);
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_order)
VALUES (2, 'codon141_table', 2);
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_order)
VALUES (3, 'codon154_table', 3);
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_order)
VALUES (4, 'codon171_table', 4);
```