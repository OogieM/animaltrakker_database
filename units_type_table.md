## units_type_table
```SQL
CREATE TABLE "units_type_table" 
	("id_unitstypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "unit_type_name" TEXT NOT NULL
	, "units_type_display_order" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_units_type_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (1,'Weight',1);
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (2,'Volume',2);
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (3,'Currency',3);
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (4,'Time',4);
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (5,'Area',5);
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (6,'Length',6);
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (7,'Count',7);
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (8,'Percent',8);
INSERT INTO units_type_table(id_unitstypeid,unit_type_name,units_type_display_order) VALUES (9,'Real Number',9);
```