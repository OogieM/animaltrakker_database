## birthing_history_table
This was the lambing_history_table
```SQL
CREATE TABLE "birthing_history_table" 
	("id_birthinghistoryid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "birthing_date" TEXT NOT NUll
	, "birthing_time" TEXT NOT NUll
	, "dam_id" INTEGER NOT NUll
	, "sire_id" INTEGER NOT NUll
	, "birthing_notes" TEXT NOT NUll
	, "animals_born" INTEGER NOT NUll
	, "animals_weaned" INTEGER NOT NUll
	, "gestation_length" INTEGER NOT NUll
	, "birthing_id_contactsid" INTEGER NOT NUll
	, "id_malebreedingid" INTEGER NOT NUll
	)
```