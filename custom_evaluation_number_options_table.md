## custom_evaluation_number_options_table
```SQL
CREATE TABLE [custom_evaluation_number_options_table]
	("id_customevaluationnumberoptionsid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_traitid" INTEGER NOT NULL
	, "custom_eval_number" Integer NOT NULL
	);
```

