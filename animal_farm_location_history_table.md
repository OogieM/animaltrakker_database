## animal_farm_location_history_table
```SQL
CREATE TABLE "animal_farm_location_history_table"
	("id_animalfarmlocationhistoryid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL
	, "id_farmlocationid" INTEGER NOT NULL
	, "farm_location_date_in" TEXT NOT NULL 
	, "farm_location_date_out" TEXT NOT NULL
	)
```