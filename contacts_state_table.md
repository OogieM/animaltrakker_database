## contacts_state_table
```SQL
CREATE TABLE "contacts_state_table" 
	("id_contactsstateid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "state_name" TEXT NOT NULL
	, "state_abbrev" TEXT NOT NULL
	, "id_contactscountryid" INTEGER NOT NULL
	, "contacts_state_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (1,'Alabama','AL',1,1);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (2,'Alaska','AK',1,2);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (3,'Arizona','AZ',1,3);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (4,'Arkansas','AR',1,4);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (5,'California','CA',1,5);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (6,'Colorado','CO',1,6);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (7,'Connecticut','CT',1,7);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (8,'Delaware','DE',1,8);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (9,'District Of Columbia','DC',1,9);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (10,'Florida','FL',1,10);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (11,'Georgia','GA',1,11);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (12,'Hawaii','HI',1,12);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (13,'Idaho','ID',1,13);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (14,'Illinois','IL',1,14);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (15,'Indiana','IN',1,15);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (16,'Iowa','IA',1,16);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (17,'Kansas','KS',1,17);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (18,'Kentucky','KY',1,18);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (19,'Louisiana','LA',1,19);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (20,'Maine','ME',1,20);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (21,'Maryland','MD',1,21);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (22,'Massachusetts','MA',1,22);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (23,'Michigan','MI',1,23);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (24,'Minnesota','MN',1,24);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (25,'Mississippi','MS',1,25);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (26,'Missouri','MO',1,26);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (27,'Montana','MT',1,27);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (28,'Nebraska','NE',1,28);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (29,'Nevada','NV',1,29);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (30,'New Hampshire','NH',1,30);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (31,'New Jersey','NJ',1,31);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (32,'New Mexico','NM',1,32);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (33,'New York','NY',1,33);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (34,'North Carolina','NC',1,34);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (35,'North Dakota','ND',1,35);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (36,'Ohio','OH',1,36);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (37,'Oklahoma','OK',1,37);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (38,'Oregon','OR',1,38);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (39,'Pennsylvania','PA',1,39);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (40,'Rhode Island','RI',1,40);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (41,'South Carolina','SC',1,41);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (42,'South Dakota','SD',1,42);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (43,'Tennessee','TN',1,43);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (44,'Texas','TX',1,44);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (45,'Utah','UT',1,45);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (46,'Vermont','VT',1,46);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (47,'Virginia','VA',1,47);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (48,'Washington','WA',1,48);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (49,'West Virginia','WV',1,49);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (50,'Wisconsin','WI',1,50);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (51,'Wyoming','WY',1,51);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (52,'Alberta','AB',2,52);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (53,'British Columbia','BC',2,53);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (54,'Manitoba','MB',2,54);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (55,'New Brunswick','NB',2,55);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (56,'Newfoundland','NL',2,56);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (57,'Northwest Territories','NT',2,57);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (58,'Nova Scotia','NS',2,58);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (59,'Nunavut','NU',2,59);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (60,'Ontario','ON',2,60);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (61,'Prince Edward Island','PE',2,61);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (62,'Quebec','QC',2,62);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (63,'Saskatchewan','SK',2,63);
INSERT INTO contacts_state_table(id_contactsstateid,state_name,state_abbrev,id_contactscountryid,contacts_state_display_order) VALUES (64,'Yukon','YT',2,64);

```