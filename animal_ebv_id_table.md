## animal_ebv_id_table
```SQL
CREATE TABLE "animal_ebv_id_table"
	("id_animalebvid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL
	, "animal_ebv_id" TEXT NOT NULL
	, "id_ebvcalculationtypeid" INTEGER NOT NULL
	)
```