## embryo_stage_table
```SQL
CREATE TABLE "embryo_stage_table" 
	("id_embryostageid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "embryo_stage_name" TEXT NOT NULL UNIQUE
	, "embryo_stage_display_order" INTEGER NOT NULL
	);
```