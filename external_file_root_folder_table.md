## external_file_root_folder_table
```SQL
CREATE TABLE "external_file_root_folder"
	( "id_externalfilerootfolderid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "absolute_path_root" TEXT NOT NULL
	);
```