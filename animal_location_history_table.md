## animal_location_history_table
```SQL
CREATE TABLE "animal_location_history_table" 
	("id_animallocationhistoryid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NUll
	, "movement_date" TEXT NOT NUll
	, "from_id_contactsid" INTEGER NOT NUll
	, "to_id_contactsid" INTEGER NOT NUll
	)
```