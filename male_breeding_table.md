---
publish: false
---
## male_breeding_table
```SQL
CREATE TABLE "male_breeding_table" 
	("id_malebreedingid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NUll
	, "date_male_in" TEXT NOT NUll
	, "time_male_in" TEXT NOT NUll
	, "date_male_out" TEXT NOT NUll
	, "time_male_out" TEXT NOT NUll
	, "id_servicetypeid" INTEGER NOT NUll
	, "id_storedsemenid" INTEGER NOT NUll
	);
```
