## genetic_characteristic_calculation_method_table
```SQL
CREATE TABLE "genetic_characteristic_calculation_method_table"
	("id_geneticcharacteristiccalculationmethodid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "genetic_characteristic_calculation_method" TEXT NOT NULL
	, "genetic_characteristic_calculation_method_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO genetic_characteristic_calculation_method_table (id_geneticcharacteristiccalculationmethodid,genetic_characteristic_calculation_method,genetic_characteristic_calculation_method_display_order) VALUES (1,'DNA Sample',1);
INSERT INTO genetic_characteristic_calculation_method_table(id_geneticcharacteristiccalculationmethodid,genetic_characteristic_calculation_method,genetic_characteristic_calculation_method_display_order) VALUES (2,'Pedigree Deduction',2);

```