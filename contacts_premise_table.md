## contacts_premise_table
```SQL
CREATE TABLE "contacts_premise_table" 
	("id_contactspremiseid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" TEXT NOT NUll
	, "id_contactsstateid" TEXT NOT NUll
	, "state_premise_id" INTEGER NOT NUll -- Boolean TRUE or 1 if the premise id is a state assigned id
	, "id_contactscountryid" TEXT NOT NUll
	, "country_premise_id" INTEGER NOT NUll -- Boolean TRUE or 1 if the premise id is a federal assigned id
	, "premise_number" TEXT NOT NUll
	, "premise_latitude" REAL NOT NUll
	, "latitude_units" INTEGER NOT NUll
	, "premise_longitude" REAL NOT NUll
	, "longitude_units" INTEGER NOT NUll
	, "premise_altitude" REAL NOT NUll
	, "altitude_units" INTEGER NOT NUll
	);
```