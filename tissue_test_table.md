## tissue_test_table
```SQL
CREATE TABLE tissue_test_table
	( "id_tissuetestid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "tissue_test_name" TEXT NOT NULL
	, "tissue_test_abbrev" TEXT NOT NULL
	, "tissue_test_display_order" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_tissue_test_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (1,1,'Flock 54 DNA Analysis','Flock54');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (2,2,'Scrapie Codons','Scrapie');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (3,3,'Illumina OvineSNP50','OvineSNP50');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (4,4,'Axiom Ovine Genotyping Array','Axiom51SNP');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (5,5,'NSIP Genomics','NSIP DNA');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (6,6,'Anaplasmosis ELISA','Anaplas');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (7,7,'Blue Tongue AGID','BT AGID');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (8,8,'Blue Tongue ELISA','BT ELISA');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (9,9,'Bovine Leukosis ELISA','BL ELISA');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (10,10,'Bovine Viral Diarrhea ELISA','BVD ELISA');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (11,11,'Brucellosi sp.','Brucellosi');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (12,12,'B. abortis','B. abortis');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (13,13,'B. canis','B. canis');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (14,14,'B. melitensis','B. melitensis');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (15,15,'B. ovis','B. ovis');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (16,16,'B. suis','B. suis');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (17,17,'Johne''s Disease ELISA','Johne''s ELISA');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (18,18,'Johne''s Disease PCR','Johne''s PCR');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (19,19,'Pseudorabies ELISA',"Pseudorabies");
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (20,20,'SRLV formerly CAE/OPP ELISA','SRLV ELISA');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (21,21,'Tritrichomonas foetus PCR Pooled','Trich Pooled');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (22,22,'Tritrichomonas foetus PCR','Trich');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (23,23,'Fecal Egg Count','FEC');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (24,24,'Fecal Egg Count McMasters','FEC M');
INSERT INTO tissue_test_table(id_tissuetestid,tissue_test_display_order,tissue_test_name,tissue_test_abbrev) VALUES (25,25,'Semen Analysis','Semen');

```