## feed_external_file_table 
This needs refactoring to newer external file structure. No one is using this at the moment so low priority.  May actually remove all feed tracking from the system as the folks that originally wanted it have moved away from detailed tracking of feed. 
```SQL
CREATE TABLE "feed_external_file_table" 
	("id_feedexternalfileid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "id_feedingredientid" INTEGER NOT NULL
	, "feed_external_file_path" TEXT NOT NULL
	, "feed_external_file_date" TEXT NOT NULL
	, "feed_external_file_time" TEXT NOT NULL
	, "id_externalfiletypeid" INTEGER NOT NULL
	);
```