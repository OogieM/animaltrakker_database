## stored_embryo_table
```SQL
CREATE TABLE "stored_embryo_table" 
	("id_storedembryoid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "sire_id" INTEGER NOT NUll
	, "dam_id" INTEGER NOT NUll
	, "number_embryos_total" INTEGER NOT NUll
	, "goblet_identifier" TEXT NOT NUll
	, "date_collected" TEXT NOT NUll
	, "time_collected" TEXT NOT NUll
	, "date_female_estrus" TEXT NOT NUll
	, "id_embryofreezingmethodid" INTEGER NOT NUll
	, "id_embryogradeid" INTEGER NOT NUll
	, "id_embryostageid" INTEGER NOT NUll
	, "id_locationcollectedid" INTEGER NOT NUll
	, "id_vetcollectedid" INTEGER NOT NUll
	, "number_embryos_per_straw" INTEGER NOT NUll
	, "id_femalebreedingid" INTEGER NOT NUll
	, "id_animalbreedid" INTEGER NOT NUll
	);
```