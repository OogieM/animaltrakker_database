## horn_type_table
```SQL
-- 	id_contactsid is a pointer to the registry that ues those horn types
-- Designed to allow different registries to ahv different names for various horn types.
CREATE TABLE "horn_type_table" 
	("id_horntypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "id_contactsid" INTEGER NOT NULL
	, "horn_type" TEXT NOT NULL
	, "horn_type_abbrev" TEXT NOT NULL
	, "horn_type_display_order" INTEGER NOT NULL
	);
```