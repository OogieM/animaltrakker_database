## sex_table
```SQL
CREATE TABLE "sex_table" 
	("id_sexid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "sex_name" TEXT NOT NULL 
	, "sex_abbrev" TEXT NOT NULL
	, "sex_standard" TEXT NOT NULL
	, "sex_abbrev_standard" TEXT NOT NULL
	, "sex_display_order" INTEGER NOT NULL
	, "id_speciesid" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_sex_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (1,'Ram','R','Male','M',1,1);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (2,'Ewe','E','Female','F',2,1);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (3,'Wether','W','Castrate','C',3,1);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (4,'Unknown','U','Unknown','U',4,1);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (5,'Buck','B','Male','M',1,2);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (6,'Doe','D','Female','F',2,2);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (7,'Wether','W','Castrate','C',3,2);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (8,'Unknown','U','Unknown','U',4,2);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (9,'Bull','B','Male','M',1,3);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (10,'Cow','C','Female','F',2,3);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (11,'Steer','S','Castrate','C',3,3);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (12,'Unknown','U','Unknown','U',4,3);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (13,'Stallion','S','Male','M',1,4);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (14,'Mare','M','Female','F',2,4);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (15,'Gelding','G','Castrate','C',3,4);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (16,'Unknown','U','Unknown','U',4,4);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (17,'Jack','JK','Male','M',1,5);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (18,'Jenny','JN','Female','F',2,5);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (19,'Gelding','G','Castrate','C',3,5);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (20,'Unknown','U','Unknown','U',4,5);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (21,'Boar','B','Male','M',1,6);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (22,'Sow','S','Female','F',2,6);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (23,'Barrow','BW','Castrate','C',3,6);
INSERT INTO sex_table(id_sexid,sex_name,sex_abbrev,sex_standard,sex_abbrev_standard,sex_display_order,id_speciesid) VALUES (24,'Unknown','U','Unknown','U',4,6);


```

![[sex_table]]