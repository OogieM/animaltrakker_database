## transfer_reason_table
```SQL
CREATE TABLE "transfer_reason_table" 
	("id_transferreasonid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "transfer_reason" TEXT NOT NULL
	, "id_contactsid" INTEGER NOT NULL
	, "transfer_reason_display_order" INTEGER NOT NULL 
	);
```

![[../09_Attachments/2021-11-16_transfer_reason_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO transfer_reason_table(id_transferreasonid,transfer_reason,transfer_reason_display_order,id_contactsid) VALUES (1,'Sold Breeding',1,1);
INSERT INTO transfer_reason_table(id_transferreasonid,transfer_reason,transfer_reason_display_order,id_contactsid) VALUES (2,'Sold Meat',2,1);
INSERT INTO transfer_reason_table(id_transferreasonid,transfer_reason,transfer_reason_display_order,id_contactsid) VALUES (3,'Sold Unregistered',3,1);
INSERT INTO transfer_reason_table(id_transferreasonid,transfer_reason,transfer_reason_display_order,id_contactsid) VALUES (4,'Natural Addition',4,1);

```