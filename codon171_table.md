## codon171_table
```SQL
CREATE TABLE "codon171_table" 
	("id_codon171id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "codon171_alleles" TEXT NOT NULL
	, "codon171_display_order" INTEGER NOT NULL
	)
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (1,'QQ',1);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (2,'Q?',5);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (3,'QR',2);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (4,'R?',9);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (5,'RR',6);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (6,'??',15);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (7,'HH',10);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (8,'QH',3);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (9,'H?',12);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (10,'RH',7);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (11,'KK',13);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (12,'QK',4);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (13,'RK',8);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (14,'K?',14);
INSERT INTO
codon171_table (id_codon171id, codon171_alleles, codon171_display_order)
VALUES (15,'HK',11);
```