## animal_breed_table
Animals can have multiple breed entries each with a percentage for composites.
```SQL
CREATE TABLE "animal_breed_table" (
	"id_animalbreedid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL
	, "id_breedid" TEXT NOT NULL
	, "breed_percentage" REAL  NOT NULL
	)
```