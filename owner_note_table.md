## owner_note_table
```SQL
-- 	Similar to the contacts_note_table but this one is for the registrar
--	to enter notes on individual owners
CREATE TABLE "owner_note_table" 
	("id_ownernoteid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "id_contactsid" INTEGER NOT NUll
	, "owner_note_date" TEXT NOT NUll
	, "owner_note_text" TEXT NOT NUll
	, "id_registry_id_contactsid" INTEGER NOT NUll
	);
```