## drug_location_table
```SQL
CREATE TABLE "drug_location_table" 
	("id_druglocationid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "drug_location_name" TEXT NOT NULL
	, "drug_location_abbrev" TEXT NOT NULL
	, "drug_location_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (1,'Subcutaneous Right Side Armpit','SQ RS Armpit',3);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (2,'Subcutaneous Left Side Armpit','SQ LS Armpit',4);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (3,'Intermuscular Neck','IM Neck',2);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (4,'Vagina','V',11);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (5,'Mouth Drench','Mouth',9);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (6,'Intermuscular Leg','IM Leg',1);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (7,'Subcutaneous Right Side Neck','SQ RS Neck',7);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (8,'Subcutaneous Left Side Neck','SQ LS Neck',8);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (9,'Intravenous','IV',10);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (10,'Subcutaneous Right Side Behind Ear','SQ RS Ear',5);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (11,'Subcutaneous Left Side Behind Ear','SQ LS Ear',6);
INSERT INTO drug_location_table(id_druglocationid,drug_location_name,drug_location_abbrev,drug_location_display_order) VALUES (12,'Eye','Eye',12);

```