## sheep_ebv_table
This table is defined based on NSIP and LambPlan EBV characteristics. For other species a separate EBV table will need to be created that tracks the EBVs that species uses.
```SQL
CREATE TABLE "sheep_ebv_table" 
	("id_sheepebvid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "sheep_id" INTEGER NOT NULL
	, "ebv_date" TEXT NOT NULL
	, "usa_maternal_index" REAL NOT NULL
	, "maternal_dollar_index" REAL NOT NULL
	, "maternal_dollar_acc" REAL NOT NULL
	, "ebv_birth_weight" REAL NOT NULL
	, "ebv_birth_weight_acc" REAL NOT NULL
	, "ebv_wean_weight" REAL NOT NULL
	, "ebv_wean_weight_acc" REAL NOT NULL
	, "ebv_maternal_birth_weight" REAL NOT NULL
	, "ebv_maternal_birth_weight_acc" REAL NOT NULL
	, "ebv_maternal_wean_weight" REAL NOT NULL
	, "ebv_maternal_wean_weight_acc" REAL NOT NULL
	, "ebv_post_wean_weight" REAL NOT NULL
	, "ebv_post_wean_weight_acc" REAL NOT NULL
	, "ebv_yearling_weight" REAL NOT NULL
	, "ebv_yearling_weight_acc" REAL NOT NULL
	, "ebv_hogget_weight" REAL NOT NULL
	, "ebv_hogget_weight_acc" REAL NOT NULL
	, "ebv_adult_weight" REAL NOT NULL
	, "ebv_adult_weight_acc" REAL NOT NULL
	, "ebv_wean_fat" REAL NOT NULL
	, "ebv_wean_fat_acc" REAL NOT NULL
	, "ebv_post_wean_fat" REAL NOT NULL
	, "ebv_post_wean_fat_acc" REAL NOT NULL
	, "ebv_yearling_fat" REAL NOT NULL
	, "ebv_yearling_fat_acc" REAL NOT NULL
	, "ebv_hogget_fat" REAL NOT NULL
	, "ebv_hogget_fat_acc" REAL NOT NULL
	, "ebv_wean_emd" REAL,"ebv_wean_emd_acc" REAL NOT NULL
	, "ebv_post_wean_emd" REAL NOT NULL
	, "ebv_post_wean_emd_acc" REAL NOT NULL
	, "ebv_yearling_emd" REAL NOT NULL
	, "ebv_yearling_emd_acc" REAL NOT NULL
	, "ebv_hogget_emd" REAL NOT NULL
	, "ebv_hogget_emd_acc" REAL NOT NULL
	, "ebv_wean_fec" REAL NOT NULL
	, "ebv_wean_fec_acc" REAL NOT NULL
	, "ebv_post_wean_fec" REAL NOT NULL
	, "ebv_post_wean_fec_acc" REAL NOT NULL
	, "ebv_yearling_fec" REAL NOT NULL
	, "ebv_yearling_fec_acc" REAL NOT NULL
	, "ebv_hogget_fec" REAL NOT NULL
	, "ebv_hogget_fec_acc" REAL NOT NULL
	, "ebv_post_wean_scrotal" REAL NOT NULL
	, "ebv_post_wean_scrotal_acc" REAL NOT NULL
	, "ebv_yearling_scrotal" REAL NOT NULL
	, "ebv_yearling_scrotal_acc" REAL NOT NULL
	, "ebv_hogget_scrotal" REAL NOT NULL
	, "ebv_hogget_scrotal_acc" REAL NOT NULL
	, "ebv_number_lambs_born" REAL NOT NULL
	, "ebv_number_lambs_born_acc" REAL NOT NULL
	, "ebv_number_lambs_weaned" REAL NOT NULL
	, "ebv_number_lambs_weaned_acc" REAL NOT NULL
	, "ebv_yearling_fleece_diameter" REAL NOT NULL
	, "ebv_yearling_fleece_diameter_acc" REAL NOT NULL
	, "ebv_hogget_fleece_diameter" REAL NOT NULL
	, "ebv_hogget_fleece_diameter_acc" REAL NOT NULL
	, "ebv_adult_fleece_diameter" REAL NOT NULL
	, "ebv_adult_fleece_diameter_acc" REAL NOT NULL
	, "ebv_yearling_greasy_fleece_weight" REAL NOT NULL
	, "ebv_yearling_greasy_fleece_weight_acc" REAL NOT NULL
	, "ebv_hogget_greasy_fleece_weight" REAL NOT NULL
	, "ebv_hogget_greasy_fleece_weight_acc" REAL NOT NULL
	, "ebv_adult_greasy_fleece_weight" REAL NOT NULL
	, "ebv_adult_greasy_fleece_weight_acc" REAL NOT NULL
	, "ebv_yearling_clean_fleece_weight" REAL NOT NULL
	, "ebv_yearling_clean_fleece_weight_acc" REAL NOT NULL
	, "ebv_hogget_clean_fleece_weight" REAL NOT NULL
	, "ebv_hogget_clean_fleece_weight_acc" REAL NOT NULL
	, "ebv_adult_clean_fleece_weight" REAL NOT NULL
	, "ebv_adult_clean_fleece_weight_acc" REAL NOT NULL
	, "ebv_yearling_fleece_yield" REAL NOT NULL
	, "ebv_yearling_fleece_yield_acc" REAL NOT NULL
	, "ebv_hogget_fleece_yield" REAL NOT NULL
	, "ebv_hogget_fleece_yield_acc" REAL NOT NULL
	, "ebv_adult_fleece_yield" REAL NOT NULL
	, "ebv_adult_fleece_yield_acc" REAL NOT NULL
	, "ebv_yearling_fiber_diameter_variation" REAL NOT NULL
	, "ebv_yearling_fiber_diameter_variation_acc" REAL NOT NULL
	, "ebv_hogget_fiber_diameter_variation" REAL NOT NULL
	, "ebv_hogget_fiber_diameter_variation_acc" REAL NOT NULL
	, "ebv_adult_fiber_diameter_variation" REAL NOT NULL
	, "ebv_adult_fiber_diameter_variation_acc" REAL NOT NULL
	, "ebv_yearling_staple_strength" REAL NOT NULL
	, "ebv_yearling_staple_strength_acc" REAL NOT NULL
	, "ebv_hogget_staple_strength" REAL NOT NULL
	, "ebv_hogget_staple_strength_acc" REAL NOT NULL
	, "ebv_adult_staple_strength" REAL NOT NULL
	, "ebv_adult_staple_strength_acc" REAL NOT NULL
	, "ebv_yearling_staple_length" REAL NOT NULL
	, "ebv_yearling_staple_length_acc" REAL NOT NULL
	, "ebv_hogget_staple_length" REAL NOT NULL
	, "ebv_hogget_staple_length_acc" REAL NOT NULL
	, "ebv_adult_staple_length" REAL NOT NULL
	, "ebv_adult_staple_length_acc" REAL NOT NULL
	, "ebv_yearling_fleece_curvature" REAL NOT NULL
	, "ebv_yearling_fleece_curvature_acc" REAL NOT NULL
	, "ebv_hogget_fleece_curvature" REAL NOT NULL
	, "ebv_hogget_fleece_curvature_acc" REAL NOT NULL
	, "ebv_adult_fleece_curvature" REAL NOT NULL
	, "ebv_adult_fleece_curvature_acc" REAL NOT NULL
	, "ebv_lambease_direct" REAL NOT NULL
	, "ebv_lambease_direct_acc" REAL NOT NULL
	, "ebv_lambease_daughter" REAL NOT NULL
	, "ebv_lambease_daughter_acc" REAL NOT NULL
	, "ebv_gestation_length" REAL NOT NULL
	, "ebv_gestation_length_acc" REAL NOT NULL
	, "ebv_gestation_length_daughter" REAL NOT NULL
	, "ebv_gestation_length_daughter_acc" REAL NOT NULL
	, "self_replacing_carcass_index" REAL NOT NULL
	, "self_replacing_carcass_acc" REAL NOT NULL
	, "self_replacing_carcass_no_repro_index" REAL NOT NULL
	, "self_replacing_carcass_no_repro_acc" REAL NOT NULL
	, "carcass_plus_index" REAL NOT NULL
	, "carcass_plus_acc" REAL NOT NULL
	, "lamb_2020_index" REAL NOT NULL
	, "lamb_2020_acc" REAL NOT NULL
	, "maternal_dollar_no_repro_index" REAL NOT NULL
	, "maternal_dollar_no_repro_acc" REAL NOT NULL
	, "dual_purpose_dollar_index" REAL NOT NULL
	, "dual_purpose_dollar_acc" REAL NOT NULL
	, "dual_purpose_dollar_no_repro_index" REAL NOT NULL
	, "dual_purpose_dollar_no_repro_acc" REAL NOT NULL
	, "which_run" TEXT NOT NULL
	, "ebv_yearling_number_lambs_born" REAL NOT NULL
	, "ebv_yearling_number_lambs_born_acc" REAL NOT NULL
	, "ebv_yearling_number_lambs_weaned" REAL NOT NULL
	, "ebv_yearling_number_lambs_weaned_acc" REAL NOT NULL
	, "border_dollar_index" REAL NOT NULL
	, "border_dollar_acc" REAL NOT NULL
	, "spare_index" REAL NOT NULL
	, "spare_index_acc" REAL NOT NULL
	, "coopworth_dollar_index" REAL NOT NULL
	, "coopworth_dollar_acc" REAL NOT NULL
	, "spare_2_index" REAL NOT NULL
	, "spare_2_index_acc" REAL NOT NULL
	, "export_index" REAL NOT NULL
	, "trade_index" REAL NOT NULL
	, "merino_dp_index" REAL NOT NULL
	, "merino_dp_acc" REAL NOT NULL
	, "merino_dp_plus_index" REAL NOT NULL
	, "merino_dp_plus_acc" REAL NOT NULL
	, "fine_medium_index" REAL NOT NULL
	, "fine_medium_acc" REAL NOT NULL
	, "fine_medium_plus_index" REAL NOT NULL
	, "fine_medium_plus_acc" REAL NOT NULL
	, "samm_index" REAL NOT NULL
	, "samm_acc" REAL NOT NULL
	, "dohne_no_repro_index" REAL NOT NULL
	, "dohne_no_repro_acc" REAL NOT NULL
	, "superfine_index" REAL NOT NULL
	, "superfine_acc" REAL NOT NULL
	, "superfine_plus_index" REAL NOT NULL
	, "superfine_plus_acc" REAL NOT NULL
	, "maternal_greasy_fleece_weight" REAL NOT NULL
	, "maternal_greasy_fleece_weight_acc" REAL NOT NULL
	, "maternal_clean_fleece_weight" REAL NOT NULL
	, "maternal_clean_fleece_weight_acc" REAL NOT NULL
	, "early_breech_wrinkle" REAL NOT NULL
	, "early_breech_wrinkle_acc" REAL NOT NULL
	, "late_breech_wrinkle" REAL NOT NULL
	, "late_breech_wrinkle_acc" REAL NOT NULL
	, "early_body_wrinkle" REAL NOT NULL
	, "early_body_wrinkle_acc" REAL NOT NULL
	, "late_body_wrinkle" REAL NOT NULL
	, "late_body_wrinkle_acc" REAL NOT NULL
	, "late_dag" REAL NOT NULL
	, "late_dag_acc" REAL NOT NULL
	, "ebv_calculation_method" INTEGER NOT NULL
	, "intramuscular_fat" REAL NOT NULL
	, "intramuscular_fat_acc" REAL NOT NULL
	, "carcass_shear_force" REAL NOT NULL
	, "carcass_shear_force_acc" REAL NOT NULL
	, "dressing_percentage" REAL NOT NULL
	, "dressing_percentage_acc" REAL NOT NULL
	, "lean_meat_yield" REAL NOT NULL
	, "lean_meat_yield_acc" REAL NOT NULL
	, "hot_carcass_weight" REAL NOT NULL
	, "hot_carcass_weight_acc" REAL NOT NULL
	, "carcass_fat" REAL NOT NULL
	, "carcass_fat_acc" REAL NOT NULL
	, "carcass_eye_muscle_depth" REAL NOT NULL
	, "carcass_eye_muscle_depth_acc" REAL NOT NULL
	);
```