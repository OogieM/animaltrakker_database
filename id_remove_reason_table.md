## id_remove_reason_table
```SQL
CREATE TABLE "id_remove_reason_table" 
	("id_idremovereasonid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_remove_reason" TEXT NOT NULL
	, "id_remove_reason_display_order" INTEGER NOT NULL
	, "id_contactsid" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_id_remove_reason_table.png]]
Insert statements to pre-fill the table with defaults. 
```SQL
INSERT INTO id_remove_reason_table(id_idremovereasonid,id_remove_reason,id_remove_reason_display_order,id_contactsid) VALUES (1,'Lost',1,1);
INSERT INTO id_remove_reason_table(id_idremovereasonid,id_remove_reason,id_remove_reason_display_order,id_contactsid) VALUES (2,'Unreadable',2,1);
INSERT INTO id_remove_reason_table(id_idremovereasonid,id_remove_reason,id_remove_reason_display_order,id_contactsid) VALUES (3,'Infection',3,1);
INSERT INTO id_remove_reason_table(id_idremovereasonid,id_remove_reason,id_remove_reason_display_order,id_contactsid) VALUES (4,'Malfunction',4,1);
INSERT INTO id_remove_reason_table(id_idremovereasonid,id_remove_reason,id_remove_reason_display_order,id_contactsid) VALUES (5,'Incompatible with Management System',5,1);
INSERT INTO id_remove_reason_table(id_idremovereasonid,id_remove_reason,id_remove_reason_display_order,id_contactsid) VALUES (6,'Wore Off',6,1);
INSERT INTO id_remove_reason_table(id_idremovereasonid,id_remove_reason,id_remove_reason_display_order,id_contactsid) VALUES (7,'Replaced',7,1);

```