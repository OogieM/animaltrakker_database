## cluster_calculation_type_table
```SQL
CREATE TABLE "cluster_calculation_type_table" 
	( "id_clustercalculationtypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "cluster_calculation_type" TEXT NOT NULL
	, "cluster_calcultion_type_display_order" INTEGER NOT NULL
	)
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO cluster_calculation_type_table(id_clustercalculationtypeid,cluster_calculation_type,cluster_calcultion_type_display_order) VALUES (1,'Wards Analysis',1);
INSERT INTO cluster_calculation_type_table(id_clustercalculationtypeid,cluster_calculation_type,cluster_calcultion_type_display_order) VALUES (2,'Pedigree Deduction',2);
INSERT INTO cluster_calculation_type_table(id_clustercalculationtypeid,cluster_calculation_type,cluster_calcultion_type_display_order) VALUES (3,'Founder Percentage',3);
INSERT INTO cluster_calculation_type_table(id_clustercalculationtypeid,cluster_calculation_type,cluster_calcultion_type_display_order) VALUES (4,'Owner Defined',4);
```