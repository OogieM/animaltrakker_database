## contacts_note_table
```SQL
CREATE TABLE "contacts_note_table" 
	("id_contactsnoteid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" INTEGER NOT NUll
	, "contact_note_date" TEXT NOT NUll
	, "contact_note_time" TEXT NOT NUll
	, "contact_note" TEXT NOT NUll
	);
```