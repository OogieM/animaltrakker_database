## animal_feed_table
```SQL
CREATE TABLE "animal_feed_table" 
	("id_animalfeedid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL 
	, "id_feedingredientid" INTEGER NOT NULL 
	, "start_date_feed" TEXT NOT NULL 
	, "end_date_feed" TEXT NOT NULL
	, "feed_serving_size" REAL NOT NULL
	, "feed_serving_size_id_unitsid" INTEGER NOT NULL
	)
```