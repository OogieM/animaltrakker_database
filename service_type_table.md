## service_type_table
```SQL
CREATE TABLE "service_type_table" 
	("id_servicetypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "service_type" TEXT NOT NULL
	, "service_abbrev" TEXT NOT NULL
	, "service_type_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults. 
```SQL
INSERT INTO service_type_table(id_servicetypeid,service_type,service_abbrev,service_type_display_order) VALUES (1,'Natural','N',1);
INSERT INTO service_type_table(id_servicetypeid,service_type,service_abbrev,service_type_display_order) VALUES (2,'Artificial Insemination Frozen Laproscopic','AI Z L',5);
INSERT INTO service_type_table(id_servicetypeid,service_type,service_abbrev,service_type_display_order) VALUES (3,'Artificial Insemination Frozen Vaginal','AI Z V',3);
INSERT INTO service_type_table(id_servicetypeid,service_type,service_abbrev,service_type_display_order) VALUES (4,'Artificial Insemination Fresh Laproscopic','AI F L',4);
INSERT INTO service_type_table(id_servicetypeid,service_type,service_abbrev,service_type_display_order) VALUES (5,'Artificial Insemination Fresh Vaginal','AI F V',2);

```