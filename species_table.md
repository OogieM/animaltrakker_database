## species_table
```SQL
CREATE TABLE "species_table" 
	("id_speciesid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "species_common_name" TEXT NOT NULL
	, "species_generic_name" TEXT NOT NULL
	, "species_scientific_family" TEXT NOT NULL
	, "species_scientific_sub_family" TEXT NOT NULL
	, "species_scientific_name" TEXT NOT NULL
	);
```

![[../09_Attachments/2021-11-16_species_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO species_table(id_speciesid,species_common_name,species_generic_name,species_scientific_name,species_scientific_family,species_scientific_sub_family) VALUES (1,'Sheep','Ovine','Ovis aries','Bovidae','Caprinae');
INSERT INTO species_table(id_speciesid,species_common_name,species_generic_name,species_scientific_name,species_scientific_family,species_scientific_sub_family) VALUES (2,'Goat','Caprine','Capra hircus','Bovidae','Caprinae');
INSERT INTO species_table(id_speciesid,species_common_name,species_generic_name,species_scientific_name,species_scientific_family,species_scientific_sub_family) VALUES (3,'Cattle','Bovine','Bos taurus','Bovidae','Bovinae');
INSERT INTO species_table(id_speciesid,species_common_name,species_generic_name,species_scientific_name,species_scientific_family,species_scientific_sub_family) VALUES (4,'Horse','Equine','Equus caballus','Equidae','');
INSERT INTO species_table(id_speciesid,species_common_name,species_generic_name,species_scientific_name,species_scientific_family,species_scientific_sub_family) VALUES (5,'Donkey','Equine','Equus asinus','Equidae','');
INSERT INTO species_table(id_speciesid,species_common_name,species_generic_name,species_scientific_name,species_scientific_family,species_scientific_sub_family) VALUES (6,'Pig','Porcine','Sus scrofa','Suidae','Suinae');

```