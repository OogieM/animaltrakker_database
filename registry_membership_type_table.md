## registry_membership_type_table
```SQL
--	id_contactsid is for the registry that this membership type applies to
CREATE TABLE "registry_membership_type_table"
	("id_registrymembershiptypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" INTEGER NOT NUll
	, "membership_type" TEXT NOT NUll
	, "membership_type_abbrev" TEXT NOT NUll
	, "membership_type_display_order" INTEGER NOT NUll
	);
```

Insert statements to pre-fill the table with defaults 
```SQL
INSERT INTO registry_membership_type_table(id_registrymembershiptypeid,id_contactsid,membership_type,membership_type_abbrev, membership_type_display_order) 
VALUES (1,1,'Full','F',1);
INSERT INTO registry_membership_type_table(id_registrymembershiptypeid,id_contactsid,membership_type,membership_type_abbrev, membership_type_display_order) 
VALUES (2,1,'Associate','A',2);
INSERT INTO registry_membership_type_table(id_registrymembershiptypeid,id_contactsid,membership_type,membership_type_abbrev, membership_type_display_order) 
VALUES (3,1,'Youth','Y',3);
INSERT INTO registry_membership_type_table(id_registrymembershiptypeid,id_contactsid,membership_type,membership_type_abbrev, membership_type_display_order) 
VALUES (4,1,'Life','Y',4);
INSERT INTO registry_membership_type_table(id_registrymembershiptypeid,id_contactsid,membership_type,membership_type_abbrev, membership_type_display_order) 
VALUES (5,1,'Overseas','Y',5);
```

![[../09_Attachments/2021-11-16_registry_membership_type_table.png]]