## animal_flock_prefix_table
```SQL
CREATE TABLE "animal_flock_prefix_table"
	("id_animalflockprefixid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL
	, "id_flockprefixid" INTEGER NOT NULL
	)
```