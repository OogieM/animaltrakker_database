## evaluation_trait_type_table
```SQL
CREATE TABLE "evaluation_trait_type_table" 
	("id_evaluationtraittypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "trait_type" TEXT NOT NULL
	, "trait_type_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults

```SQL
INSERT INTO evaluation_trait_type_table(id_evaluationtraittypeid,trait_type,trait_type_display_order) VALUES (1,'Score 1 to 5',1);
INSERT INTO evaluation_trait_type_table(id_evaluationtraittypeid,trait_type,trait_type_display_order) VALUES (2,'Real Value',2);
INSERT INTO evaluation_trait_type_table(id_evaluationtraittypeid,trait_type,trait_type_display_order) VALUES (3,'User List',3);
INSERT INTO evaluation_trait_type_table(id_evaluationtraittypeid,trait_type,trait_type_display_order) VALUES (4,'Calculated Real Value',4);

```