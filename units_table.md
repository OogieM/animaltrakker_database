## units_table
```SQL
CREATE TABLE "units_table" 
	("id_unitsid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "units_name" TEXT NOT NULL
	, "units_abbrev" TEXT NOT NULL
	, "id_unitstypeid" INTEGER NOT NULL
	, "units_display_order" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_units_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (1,'Decimal Pounds','lbs',1,1);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (2,'Kilograms','kg',1,2);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (3,'Inches','in',6,6);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (4,'Centimeters','cm',6,10);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (5,'Days','d',4,4);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (6,'Hours','h',4,5);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (7,'Micron','u',6,13);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (8,'US Dollars','$',3,5);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (9,'Acres','Ac',5,15);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (10,'Tons','t',1,3);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (11,'Hectares','ha',5,14);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (12,'milli-International Unit per milliliter','mIU/ml',7,4);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (13,'Square Inches','sq in',5,16);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (14,'Square Centimeters','sq cm',5,17);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (15,'Milliliter','ml',2,12);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (16,'UK Currency Pounds','L',3,6);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (17,'Feet','ft',6,11);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (18,'Eggs per gram','epg',7,18);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (19,'Forward Progressive Motility %','FPM',8,19);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (20,'Normal  Morphology %','Morph',8,20);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (21,'Years','y',4,1);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (22,'Months','m',4,2);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (23,'Weeks','w',4,3);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (24,'Seconds','s',4,6);
INSERT INTO units_table(id_unitsid,units_name,units_abbrev,id_unitstypeid,units_display_order) VALUES (25,'Decimal Value','Value',9,9);
```