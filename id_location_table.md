## id_location_table
```SQL
CREATE TABLE "id_location_table" 
	("id_idlocationid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_location_name" TEXT NOT NULL DEFAULT ('Right Ear')
	, "id_location_abbrev" TEXT (2) NOT NULL DEFAULT ('RE')
	, "id_location_display_order" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_id_location_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (1,'Right Ear','R',1);
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (2,'Left Ear','L',2);
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (3,'Right Flank','RF',3);
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (4,'Left Flank','LF',4);
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (5,'Side','S',5);
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (6,'Unknown','U',7);
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (7,'Tail Web','TW',6);
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (8,'Right Neck','RN',8);
INSERT INTO id_location_table(id_idlocationid,id_location_name,id_location_abbrev,id_location_display_order) VALUES (9,'Left Neck','LN',9);

```