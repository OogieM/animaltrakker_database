## registry_membership_status_table
```SQL
--	id_contactsid is for the registry that this membership status applies to
CREATE TABLE "registry_membership_status_table" 
	("id_registrymembershipstatusid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_contactsid" INTEGER NOT NUll
	, "membership_status" TEXT NOT NUll
	, "membership_status_abbrev" TEXT NOT NUll
	, "membership_status_display_order" INTEGER NOT NUll
	);
```

Insert statements to pre-fill the table with defaults 
```SQL
INSERT INTO registry_membership_status_table(id_registrymembershipstatusid,id_contactsid,membership_status,membership_status_abbrev, membership_status_display_order) 
VALUES (1,1,'On Hold','H',2);
INSERT INTO registry_membership_status_table(id_registrymembershipstatusid,id_contactsid,membership_status,membership_status_abbrev, membership_status_display_order) 
VALUES (2,1,'Active','A',1);
INSERT INTO registry_membership_status_table(id_registrymembershipstatusid,id_contactsid,membership_status,membership_status_abbrev, membership_status_display_order) 
VALUES (3,1,'Resigned','R',3);

```

![[../09_Attachments/2021-11-16_registry_membership_status_table.png]]