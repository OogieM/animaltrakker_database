## id_type_table
```SQL
CREATE TABLE "id_type_table" 
	("id_idtypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_type_name" TEXT NOT NULL
	, "id_type_abbrev" TEXT NOT NULL
	, "id_type_display_order" INTEGER NOT NULL
	);
```

![[../09_Attachments/2021-11-16_id_type_table.png]]
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (1,'Federal','Fed',4);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (2,'Electronic','EID',1);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (3,'Paint','Pnt',6);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (4,'Farm','Farm',2);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (5,'Tattoo','Tat',9);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (6,'Split','Sp',8);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (7,'Notch','Nch',7);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (8,'Name','Name',5);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (9,'Freeze Brand','Fz',10);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (10,'Trich','Tr',3);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (11,'NUES','NUES',11);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (12,'Sale Order','Sale',12);
INSERT INTO id_type_table(id_idtypeid,id_type_name,id_type_abbrev,id_type_display_order) VALUES (13,'Bangs','Bangs',13);

```