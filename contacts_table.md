## contacts_table
```SQL
CREATE TABLE "contacts_table" 
	("id_contactsid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NUll UNIQUE
	, "contact_last_name" TEXT NOT NUll
	, "contact_first_name" TEXT NOT NUll
	, "contact_middle_name" TEXT NOT NUll
	, "contact_title" INTEGER NOT NUll
	, "contact_company" TEXT NOT NUll
	, "contact_physical_address1" TEXT NOT NUll
	, "contact_physical_address2" TEXT NOT NUll
	, "contact_physical_city" TEXT NOT NUll
	-- state is a pointer into the contacts_state_table 
	-- It's named here because of the different physical and mailing addresses
	, "contact_physical_state" INTEGER NOT NUll 
	, "contact_physical_postcode" TEXT NOT NUll
	-- country is a pointer into the contacts_country_table 
	-- It's named here because of the different physical and mailing addresses
	, "id_contacts_physical_id_countryid" INTEGER NOT NUll
	, "id_contacts_physical_id_countyid" INTEGER NOT NULL 
	, "contact_mailing_address1" TEXT NOT NUll
	, "contact_mailing_address2" TEXT NOT NUll
	, "contact_mailing_city" TEXT NOT NUll
	, "contact_mailing_state" INTEGER NOT NUll
	, "contact_mailing_postcode" TEXT NOT NUll
	, "id_contacts_mailing_id_countryid" INTEGER NOT NUll
	, "id_contacts_mailing_id_countyid" INTEGER NOT NULL
	, "contact_main_phone" TEXT NOT NUll
	, "contact_cell_phone" TEXT NOT NUll
	, "contact_fax" TEXT NOT NUll
	, "contact_email1" TEXT NOT NUll
	, "contact_email2" TEXT NOT NUll
	, "contact_website" TEXT NOT NUll
	);
```

Add an Unknown contact for animal breed table. Add in all current vet contacts and AnimalTrakker support. 

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO contacts_table(id_contactsid,contact_last_name) 
VALUES (1,'Unknown','','','',''
		,''.''.''.''.''.''.''
		,''.''.''.''.''.''.''
		,'','',''
		,'','','');
```
