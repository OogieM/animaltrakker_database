## animal_id_info_table
```SQL
CREATE TABLE "animal_id_info_table" 
	("id_animalidinfoid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "id_animalid" INTEGER NOT NUll
	, "id_tagtypeid" INTEGER NOT NUll
	, "id_male_id_tagcolorid" INTEGER NOT NUll
	, "id_female_id_tagcolorid" INTEGER NOT NUll
	, "id_taglocationid" INTEGER NOT NUll
	, "tag_date_on" TEXT NOT NUll
	, "tag_time_on" TEXT NOT NUll
	, "tag_date_off" TEXT NOT NUll
	, "tag_time_off" TEXT NOT NUll
	, "tag_number" TEXT NOT NUll
	, "id_flockid" INTEGER NOT NUll
	, "official_id" INTEGER NOT NUll
	, "id_removetagreasonid" INTEGER NOT NUll
	);
```