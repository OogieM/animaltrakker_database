## predefined_notes_table
```SQL
CREATE TABLE "predefined_notes_table" 
	("id_predefinednotesid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "predefined_note_text" TEXT NOT NULL
	, "id_contactsid" INTEGER NOT NULL
	, "predefined_note_display_order" INTEGER NOT NULL
	);
```

