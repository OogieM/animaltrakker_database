## lab_test_external_submission_form_table
```SQL
CREATE TABLE "lab_test_external_submission_form_table" 
	("id_labtestexternalsubmissionformid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_externalfiletypeid" TEXT NOT NULL
	, "id_tissuetestid" TEXT NOT NULL
	, "id_contactsid" TEXT NOT NULL
	, "lab_submission_form_filename" TEXT NOT NULL
	);
```

Insert statements to fill the form with the Colorado lab reports for major tests This is a dummy record. 
```SQL
INSERT INTO 
lab_test_external_submission_form_table(id_labtestexternalsubmissionformid,id_externalfiletypeid,id_tissuetestid,id_contactsid,lab_submission_form_filename) 
VALUES (1,0,0);

```
