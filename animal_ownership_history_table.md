## animal_ownership_history_table
```SQL
CREATE TABLE "animal_ownership_history_table" 
	("id_animalownershiphistoryid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NUll
	, "transfer_date" TEXT NOT NUll
	, "from_id_contactsid" INTEGER NOT NUll
	, "to_id_contactsid" INTEGER NOT NUll
	, "id_transferreasonid" INTEGER NOT NUll
	, "sell_price" REAL NOT NUll
	, "sell_price_id_unitsid" INTEGER NOT NUll
	)
```