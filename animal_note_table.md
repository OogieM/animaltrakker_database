## animal_note_table
```SQL
CREATE TABLE "animal_note_table" 
	("id_animalnoteid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NUll
	, "note_text" TEXT NOT NUll
	, "note_date" TEXT NOT NUll
	, "note_time" TEXT NOT NUll
	, "id_predefinednotesid" INTEGER NOT NUll
	)
```