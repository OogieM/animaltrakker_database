## animals_born_table
This was the lambs-born_table. It contains details about that particular parturition and how the offspring were raised. This perhaps could be combined into the main animal table. 
```SQL
CREATE TABLE "animals_born_table" 
	("id_animalsbornid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_birthinghistoryid" INTEGER NOT NUll
	, "id_animalid" INTEGER NOT NULL 
	, "birth_order" INTEGER NOT NULL 
	, "id_fosterdamid" INTEGER NOT NUll
	, "hand_reared" INTEGER NOT NUll
	)
```