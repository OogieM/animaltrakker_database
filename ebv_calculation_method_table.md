## ebv_calculation_method_table
```SQL
CREATE TABLE "ebv_calculation_method_table"
	("id_ebvcalculationmethodid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "ebv_calculation_method" TEXT NOT NULL
	, "ebv_calculation_method_display_order" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO ebv_calculation_method_table(id_ebvcalculationmethodid, ebv_calculation_method,ebv_calculation_method_display_order) 
VALUES (1,'NSIP',3);
INSERT INTO ebv_calculation_method_table(id_ebvcalculationmethodid, ebv_calculation_method,ebv_calculation_method_display_order) 
VALUES (2,'LambPlan',2);
INSERT INTO ebv_calculation_method_table(id_ebvcalculationmethodid, ebv_calculation_method,ebv_calculation_method_display_order) 
VALUES (3,'BVest',1);
INSERT INTO ebv_calculation_method_table(id_ebvcalculationmethodid, ebv_calculation_method,ebv_calculation_method_display_order) 
VALUES (4,'Signet',4);
```