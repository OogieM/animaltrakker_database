## animal_evaluation_table
```SQL
--	There are 3 types of traits that can be evaluated.
--		Scored Traits
--		Real Traits
--		Custom Traits
-- 	trait_name01 through trait_name20 fields are links into the evaluation_trait_table
-- 	trait_score16 through trait_score20 are links into the custom_evaluation_traits_table
--	Scored traits are subjective and have a range from 1 to 5 with 5 being best
-- 	Example: White nose 5 is a black nose 1 is a terrible white nose
--	Real traits are measured and the answers are numerical
--	Example: scrotal circumference is measured in centimeters
--	Real traits also have a units associated with them. 
--	Example: inches, pounds, centimeters or eggs per gram
-- 	Custom traits have answers that are determined by the set-up
--	Example: Trait is Udder Size 
--		Options are No Udder, Small Udder, Medium Udder and Large Udder
CREATE TABLE "animal_evaluation_table" 
	("id_animalevaluationid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NULL 
	, "trait_name01" INTEGER NOT NULL
	, "trait_score01" REAL NOT NULL
	, "trait_name02" INTEGER NOT NULL
	, "trait_score02" REAL NOT NULL
	, "trait_name03" INTEGER NOT NULL
	, "trait_score03" REAL NOT NULL
	, "trait_name04" INTEGER NOT NULL
	, "trait_score04" REAL NOT NULL
	, "trait_name05" INTEGER NOT NULL
	, "trait_score05" REAL NOT NULL
	, "trait_name06" INTEGER NOT NULL
	, "trait_score06" REAL NOT NULL
	, "trait_name07" INTEGER NOT NULL
	, "trait_score07" REAL NOT NULL
	, "trait_name08" INTEGER NOT NULL
	, "trait_score08" REAL NOT NULL
	, "trait_name09" INTEGER NOT NULL
	, "trait_score09" REAL NOT NULL
	, "trait_name10" INTEGER NOT NULL
	, "trait_score10" REAL NOT NULL
	, "trait_name11" INTEGER NOT NULL
	, "trait_score11" REAL NOT NULL
	, "trait_name12" INTEGER NOT NULL
	, "trait_score12" REAL NOT NULL
	, "trait_name13" INTEGER NOT NULL
	, "trait_score13" REAL NOT NULL
	, "trait_name14" INTEGER NOT NULL
	, "trait_score14" REAL NOT NULL
	, "trait_name15" INTEGER NOT NULL
	, "trait_score15" REAL NOT NULL
	, "trait_units11" INTEGER NOT NULL
	, "trait_units12" INTEGER NOT NULL
	, "trait_units13" INTEGER NOT NULL
	, "trait_units14" INTEGER NOT NULL
	, "trait_units15" INTEGER NOT NULL
	, "trait_name16" INTEGER NOT NULL
	, "trait_score16" INTEGER NOT NULL
	, "trait_name17" INTEGER NOT NULL
	, "trait_score17" INTEGER NOT NULL
	, "trait_name18" INTEGER NOT NULL
	, "trait_score18" INTEGER NOT NULL
	, "trait_name19" INTEGER NOT NULL
	, "trait_score19" INTEGER NOT NULL
	, "trait_name20" INTEGER NOT NULL
	, "trait_score20" INTEGER NOT NULL
	, "animal_rank" INTEGER NOT NULL
	, "number_animals_ranked" INTEGER NOT NULL
	, "eval_date" TEXT NOT NULL
	, "eval_time" TEXT NOT NULL
	, "age_in_days" INTEGER NOT NULL
	)
```