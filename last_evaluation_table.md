## last_evaluation_table
place to store the last set of evaluation criteria so we can reload it faster.
```SQL
CREATE TABLE "last_evaluation_table" 
	("id_lastevaluationid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "id_traitid" INTEGER NOT NULL
	, "id_unitsid" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults. Set everything to zeros. Only 20 entries allowed in this table. That's the total number of evaluation items.
```SQL
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (1,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (2,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (3,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (4,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (5,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (6,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (7,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (8,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (9,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (10,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (11,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (12,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (13,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (14,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (15,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (16,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (17,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (18,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (19,0,0);
INSERT INTO last_evaluation_table(id_lastevaluationid,id_traitid,id_unitsid) VALUES (20,0,0);

```