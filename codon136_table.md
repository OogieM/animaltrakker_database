## codon136_table
```SQL
CREATE TABLE "codon136_table" 
	("id_codon136id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "codon136_alleles" TEXT NOT NULL
	, "codon136_display_order" INTEGER NOT NULL
	)
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO
codon136_table (id_codon136id, codon136_alleles, codon136_display_order)
VALUES (1, "AA",1);
INSERT INTO
codon136_table (id_codon136id, codon136_alleles, codon136_display_order)
VALUES (2, "A?",2);
INSERT INTO
codon136_table (id_codon136id, codon136_alleles, codon136_display_order)
VALUES (3, "AV",3);
INSERT INTO
codon136_table (id_codon136id, codon136_alleles, codon136_display_order)
VALUES (4, "V?",4);
INSERT INTO
codon136_table (id_codon136id, codon136_alleles, codon136_display_order)
VALUES (5, "VV",5);
INSERT INTO
codon136_table (id_codon136id, codon136_alleles, codon136_display_order)
VALUES (6, "??",6);
```