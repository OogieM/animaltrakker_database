## animal_tissue_sample_taken_table
```SQL
CREATE TABLE "animal_tissue_sample_taken_table"
	( "id_animaltissuesampletakenid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "id_animalid" INTEGER NOT NUll
	, "id_tissuesampletypeid" INTEGER NOT NUll
	, "tissue_sample_date" TEXT NOT NUll
	, "tissue_sample_time" TEXT NOT NUll
	, "id_tissuecontainertypeid" INTEGER NOT NUll
	, "tissue_sample_container_id" TEXT NOT NUll
	, "tissue_sample_container_exp_date" TEXT NOT NUll
	)
```