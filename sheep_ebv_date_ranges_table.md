## sheep_ebv_date_ranges_table
```SQL
CREATE TABLE "sheep_ebv_date_ranges_table" 
	("id_sheepebvdaterangesid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE 
	, "age_name" TEXT NOT NULL
	, "lambplan_minimum_days" INTEGER NOT NULL
	, "lambplan_maximum_days" INTEGER NOT NULL
	, "nsip_minimum_days" INTEGER NOT NULL
	, "nsip_maximum_days" INTEGER NOT NULL
	, "user_minimum_days" INTEGER NOT NULL
	, "user_maximum_days" INTEGER NOT NULL
	, "lambplan_optimal_days" INTEGER NOT NULL
	, "nsip_optimal_days" INTEGER NOT NULL
	, "user_optimal_days" INTEGER NOT NULL
	);
```

Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('1','Birth','0','1','0','1','0','1','1','1','1');
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('2','Weaning','42','120','40','120','40','120','80','60','50'
);
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('3','Early Post Weaning','120','210','120','210','120','210','','','120');
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('4','Postweaning','210','300','160','340','160','340','','','250');
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('5','Yearling','300','400','290','430','290','430','','','365');
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('6','Hogget','400','540','410','550','420','550','','','470');
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('7','Adult','421','1107','421','1107','421','1107','','','730');
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('8','Adult3','655','1549','655','1549','655','1549','','','1095');
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('9','Adult4','918','1990','918','1990','918','1990','','','1460');
INSERT INTO sheep_ebv_date_ranges_table (id_sheepebvdaterangesid, age_name, lambplan_minimum_days, lambplan_maximum_days, nsip_minimum_days, nsip_maximum_days, user_minimum_days, user_maximum_days, lambplan_optimal_days, nsip_optimal_days, user_optimal_days)
VALUES ('10','Adult5','1181','2553','1181','2553','1181','2553','','','1825');

```