## veterinarian_license_type_table
```SQL
CREATE TABLE "veterinarian_license_type_table" 
	("id_veterinarianlicensetypeid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE
	, "vet_license_type" TEXT NOT NUll
	);
```
	
Insert statements to pre-fill the table with defaults
```SQL
INSERT INTO veterinarian_license_type_table(vet_license_type) VALUES ('USDA LEVEL 1');
INSERT INTO veterinarian_license_type_table(vet_license_type) VALUES ('USDA LEVEL 2');
INSERT INTO veterinarian_license_type_table(vet_license_type) VALUES ('STATE');
```